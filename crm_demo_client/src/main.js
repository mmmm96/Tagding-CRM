import babelpolyfill from 'babel-polyfill'
import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import VueRouter from 'vue-router'
import store from './vuex/store'
import Vuex from 'vuex'
import routes from './routes'
import util from './common/util'
import PicUpload from './pages/ctrl/PicUpload.vue'
import DocUpload from './pages/ctrl/DocUpload.vue'
import PicCardUpload from './pages/ctrl/PicCardUpload.vue'
import Select from './pages/ctrl/Select.vue'
import Checkbox from './pages/ctrl/Checkbox.vue'
import DataPicker from './pages/ctrl/DataPicker.vue'
import BaseList from './pages/BaseList.vue'


//import Mock from './mock';
//Mock.bootstrap();

//全局变量统一在此定义
global.debug = true
if (process.env.NODE_ENV == 'development') {
    //global.serverHost = "http://127.0.0.1:8056/"
   // global.serverHost = "http://demo.hz768.com/"
    global.serverHost = "http://127.0.0.1:8055/"
}else{
   global.serverHost = "http://crm.nn3600.com/"
}
global.bus = new Vue()
global.user = null
global.roles = null
global.allRoutes = null
global.Module=null
global.nowPath = ""
global.sysParam = {}
Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

//注册全局组件
Vue.component("sy-pic-upload",PicUpload)
Vue.component("sy-doc-upload",DocUpload)
Vue.component("sy-card-upload",PicCardUpload)
Vue.component("sy-baselist",BaseList)
Vue.component("sy-select",Select)
Vue.component("sy-checkbox",Checkbox)
Vue.component("sy-dataPicker",DataPicker)

Vue.filter("formatDate", function(value) {   //全局方法 Vue.filter() 注册一个自定义过滤器,必须放在Vue实例化前面
    return util.date.format(value,"yyyy-MM-dd");
});

Vue.filter("formatDate2", function(value) {   //全局方法 Vue.filter() 注册一个自定义过滤器,必须放在Vue实例化前面
    return util.date.format(value,"yyyy-MM-dd hh:mm:ss");
});

function routerMap(item) {
        if (item.componentPath) {
            item.component = resolve => require(["./pages/" + item.componentPath], resolve) //动态加载
        }
        if(item.children){
            item.children.map(routerMap)
        }
        return item
}
const router = new VueRouter()
util.get("server/system/Main.ashx?cmd=Init&r="+Math.random(),{},null,result=> {
    global.sysParam =  util.date.formatObj(result.sysParam)
    global.user = result.user
    global.roles = result.roles
    global.allRoutes = result.menu.concat(routes).map(routerMap)
    global.Module = result.module
    util.handleAuthority(result.authority)
    router.addRoutes(global.allRoutes)
    init();
})

//心跳包
setInterval(()=>{
       util.get("Portal.aspx?cmd=Ticket")
       global.bus.$emit('RefreshMsgCount')
    },
5*1000*60)

function init() {
    router.beforeEach((to, from, next) => {
        if (to.path == '/login') {
            global.user = null
        }
        if (!global.user && to.path != '/login') {
            next({ path: '/login' })
        } else {
            //判断查询权限
            global.nowPath = to.path
            if(util.hasAuth("view")){
                next()
            }else{
                next({ path: '/login' })
            }
        }
    })

    //不必在全局注册每个组件。通过使用组件实例选项注册，可以使组件仅在另一个实例/组件的作用域中可用：
    global.vueRef = new Vue({
        el: '#app',
        template: '<App></App>',
        router,
        store,
        components: {App: App }
    })
}



