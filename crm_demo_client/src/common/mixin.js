
import util from './util'
import NProgress from 'nprogress'//页面顶部进度条

export default {
        methods: {
            loadFn(key){
                let _self = this
                return function (flag) {
                    _self[key] = flag
                }
            },
            progressFn(flag){
                if(flag){
                    NProgress.start();
                }else{
                    NProgress.done();
                }
            },
            formatDate(row, column){
                if (row[column.property]) {
                    return util.date.format(row[column.property], "yyyy-MM-dd")
                } else {
                    return ""
                }
            },
            formatDate2(row, column){
                if (row[column.property]) {
                    return util.date.format(row[column.property], "yyyy-MM-dd hh:mm:ss")
                } else {
                    return ""
                }
            },
            //性别显示转换
            formatSex(row, column) {
                return row[column.property] == 1 ? '男' : row[column.property]  == 0 ? '女' : '未知';
            },
            formatAge(row, column) {
                return 38
            },
            isFloat(rule, value, callback){
                var re = /^\-?[0-9]+\.?[0-9]*$/
                if (value && !re.test(value))
                {
                    callback(new Error('请输入数字'));
                }else {
                    callback();
                }
            }
        },
        mounted(){

        }
}