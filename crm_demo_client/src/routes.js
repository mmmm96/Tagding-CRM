import Login from './pages/Login.vue'
import NotFound from './pages/404.vue'
import Home from './pages/Home.vue'
import Main from './pages/Main.vue'
import Menu from './pages/system/Menu.vue'
import Test from './pages/Test.vue'

const routes = [
    {
        path: '/login',
        component: Login,
        name: 'login',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '404',
        hidden: true
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '主页',
        iconCls: 'el-icon-message',//图标样式class
        hidden:true,
        children: [
            { path: '/Test', component: Test, name: '测试', hidden:false},
            { path: '/Menu', component: Menu, name: 'Menu', hidden:false},
            { path: '/Main', component: Main, name: '消息中心', hidden:true}
        ]
    },
    {
        path: '*',
        name: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;