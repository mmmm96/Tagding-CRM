﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_apply
    {

        /// <summary>
        /// 主键
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 添加时间
        /// </summary>
        public virtual DateTime FAddTime
        {
            get;
            set;
        }
        /// <summary>
        /// 申购时间
        /// </summary>
        public virtual DateTime FApplyTime
        {
            get;
            set;
        }
        /// <summary>
        /// 编号
        /// </summary>
        public virtual string FApplyNo
        {
            get;
            set;
        }
        /// <summary>
        /// 项目ID
        /// </summary>
        public virtual int FProjectID
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get;
            set;
        }
        /// <summary>
        /// 收货地址
        /// </summary>
        public virtual string FShippingAddress
        {
            get;
            set;
        }
        /// <summary>
        /// 审批人
        /// </summary>
        public virtual string FApproveUser
        {
            get;
            set;
        }
        /// <summary>
        /// 审批时间
        /// </summary>
        public virtual DateTime FApproveTime
        {
            get;
            set;
        }
        /// <summary>
        /// 要求到货时间
        /// </summary>
        public virtual DateTime FArriveDate
        {
            get;
            set;
        }
        /// <summary>
        /// 申购单总价
        /// </summary>
        public virtual decimal FTotalPrice
        {
            get;
            set;
        }
        /// <summary>
        /// 申请人
        /// </summary>
        public virtual string FApplyUser
        {
            get;
            set;
        }
        /// <summary>
        /// 审核步骤
        /// </summary>
        public virtual int FApproveStep
        {
            get;
            set;
        }
        /// <summary>
        /// 审核状态
        /// </summary>
        public virtual int FApproveStatus
        {
            get;
            set;
        }
        /// <summary>
        /// 下一步审核人ID
        /// </summary>
        public virtual int FToApprover
        {
            get;
            set;
        }
        /// <summary>
        /// 复核人
        /// </summary>
        public virtual string FCheckUser
        {
            get;
            set;
        }
        /// <summary>
        /// 复核时间
        /// </summary>
        public virtual DateTime FCheckTime
        {
            get;
            set;
        }
        /// <summary>
        /// 提交时间
        /// </summary>
        public virtual DateTime FSubmitTime
        {
            get;
            set;
        }       
                 
        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual c_project Project
        {
            get;
            set;
        }

        public virtual string ProjectName
        {
            get
            {
                return POTool.Str(() => { return Project == null ? "" : Project.FName; });
            }
        }

        [ScriptIgnore]
        public virtual t_user User
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get
            {
                return POTool.Str(() => { return User == null ? "" : User.FName; });
            }
        }
    }
}