﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_stonehouseflow_detail
        public class c_stonehouseflow_detail
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FProductID
        /// </summary>
        public virtual int FProductID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FQuantity
        /// </summary>
        public virtual decimal FQuantity
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FStonehouseflowID
        /// </summary>
        public virtual int FStonehouseflowID
        {
            get; 
            set; 
        }        
                 
        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual c_product Product
        {
            get;
            set;
        }

        public virtual string ProductName
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.FName; });
            }
        }

        public virtual string ProductModel
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.FModel; });
            }
        }

        public virtual string ProductUnitName
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.UnitName; });
            }
        }
    }
}