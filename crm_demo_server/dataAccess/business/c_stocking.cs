﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_stocking
        public class c_stocking
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FTitle
        /// </summary>
        public virtual string FTitle
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FAddTime
        /// </summary>
        public virtual DateTime FAddTime
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FUserID
        /// </summary>
        public virtual int FUserID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FNote
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FStonehouseID
        /// </summary>
        public virtual int FStonehouseID
        {
            get; 
            set; 
        }        
                 
        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual c_stonehouse Stonehouse
        {
            get;
            set;
        }

        public virtual string StonehouseName
        {
            get
            {
                return POTool.Str(() => { return Stonehouse == null ? "" : Stonehouse.FName; });
            }
        }

        [ScriptIgnore]
        public virtual t_user User
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get
            {
                return POTool.Str(() => { return User == null ? "" : User.FName; });
            }
        }
    }
}