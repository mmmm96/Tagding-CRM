﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_apply_detail
        public class c_apply_detail
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FProductID
        /// </summary>
        public virtual int FProductID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FQuantity
        /// </summary>
        public virtual decimal FQuantity
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FNote
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FApplyID
        /// </summary>
        public virtual int FApplyID
        {
            get; 
            set; 
        }
        /// <summary>
        /// FPrice
        /// </summary>
        public virtual decimal FPrice
        {
            get;
            set;
        }     
        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual c_product Product
        {
            get;
            set;
        }

        public virtual string ProductName
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.FName; });
            }
        }

        public virtual string ProductModel
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.FModel; });
            }
        }

        public virtual string ProductUnitName
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.UnitName; });
            }
        }
    }
}