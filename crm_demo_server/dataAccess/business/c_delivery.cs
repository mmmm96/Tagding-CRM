﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_delivery
        public class c_delivery
    {

        /// <summary>
        /// 主键
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 添加时间
        /// </summary>
        public virtual DateTime FAddTime
        {
            get;
            set;
        }
        /// <summary>
        /// 发货时间
        /// </summary>
        public virtual DateTime FDeliveryTime
        {
            get;
            set;
        }
        /// <summary>
        /// 申购单编号
        /// </summary>
        public virtual string FApplyNo
        {
            get;
            set;
        }
        /// <summary>
        /// 项目ID
        /// </summary>
        public virtual int FProjectID
        {
            get;
            set;
        }
        /// <summary>
        /// 发货人
        /// </summary>
        public virtual string FDeliveryUser
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get;
            set;
        }
        /// <summary>
        /// 复核人
        /// </summary>
        public virtual string FCheckUser
        {
            get;
            set;
        }
        /// <summary>
        /// 复核信息
        /// </summary>
        public virtual string FCheckNote
        {
            get;
            set;
        }
        /// <summary>
        /// 审批人
        /// </summary>
        public virtual string FApproveUser
        {
            get;
            set;
        }
        /// <summary>
        /// 审批信息
        /// </summary>
        public virtual string FApproveNote
        {
            get;
            set;
        }
        /// <summary>
        /// 收货人
        /// </summary>
        public virtual int FShippingUser
        {
            get;
            set;
        }
        /// <summary>
        /// 收货地址
        /// </summary>
        public virtual string FShippingAddress
        {
            get;
            set;
        }
        /// <summary>
        /// 状态
        /// </summary>
        public virtual int FApproveStatus
        {
            get;
            set;
        }
        /// <summary>
        /// 复核时间
        /// </summary>
        public virtual DateTime FCheckTime
        {
            get;
            set;
        }
        /// <summary>
        /// 审核步骤
        /// </summary>
        public virtual int FApproveStep
        {
            get;
            set;
        }
        /// <summary>
        /// 下一步审核人ID
        /// </summary>
        public virtual int FToApprover
        {
            get;
            set;
        }
        /// <summary>
        /// 提交时间
        /// </summary>
        public virtual DateTime FSubmitTime
        {
            get;
            set;
        }
        /// <summary>
        /// 审批时间
        /// </summary>
        public virtual DateTime FApproveTime
        {
            get;
            set;
        }        
                 
         /// <summary>
        /// 是否确认收货,0:未确认,1:已确认
        /// </summary>
        public virtual int FIsConfirm
        {
            get;
            set;
        }
           
        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual c_project Project
        {
            get;
            set;
        }

        public virtual string ProjectName
        {
            get
            {
                return POTool.Str(() => { return Project == null ? "" : Project.FName; });
            }
        }

        [ScriptIgnore]
        public virtual t_user User
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get
            {
                return POTool.Str(() => { return User == null ? "" : User.FName; });
            }
        }

        [ScriptIgnore]
        public virtual t_user CheckUser
        {
            get;
            set;
        }

        public virtual string CheckUserName
        {
            get
            {
                return POTool.Str(() => { return CheckUser == null ? "" : CheckUser.FName; });
            }
        }

        [ScriptIgnore]
        public virtual t_user ApproveUser
        {
            get;
            set;
        }

        public virtual string ApproveUserName
        {
            get
            {
                return POTool.Str(() => { return ApproveUser == null ? "" : ApproveUser.FName; });
            }
        }

        [ScriptIgnore]
        public virtual t_user ShippingUser
        {
            get;
            set;
        }

        public virtual string ShippingUserName
        {
            get
            {
                return POTool.Str(() => { return ShippingUser == null ? "" : ShippingUser.FName; });
            }
        }
    }
}