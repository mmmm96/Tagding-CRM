﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_stock
        public class c_stock
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FStonehouseID
        /// </summary>
        public virtual int FStonehouseID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FProductID
        /// </summary>
        public virtual int FProductID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FQuantity
        /// </summary>
        public virtual decimal FQuantity
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FNote
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
                 
        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual c_product Product
        {
            get;
            set;
        }

        public virtual string ProductName
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.FName; });
            }
        }

        public virtual string ProductModel
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.FModel; });
            }
        }

        public virtual string ProductUnit
        {
            get
            {
                return POTool.Str(() => { return Product == null ? "" : Product.UnitName; });
            }
        }

        [ScriptIgnore]
        public virtual c_stonehouse Stonehouse
        {
            get;
            set;
        }

        public virtual string StonehouseName
        {
            get
            {
                return POTool.Str(() => { return Stonehouse == null ? "" : Stonehouse.FName; });
            }
        }
    }
}