﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_product
        public class c_product
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FName
        /// </summary>
        public virtual string FName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FNote
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FTypeID
        /// </summary>
        public virtual int FTypeID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FModel
        /// </summary>
        public virtual string FModel
        {
            get; 
            set; 
        }


        /// <summary>
        /// FUnitID
        /// </summary>
        public virtual int FUnitID
        {
            get;
            set;
        }

        /// <summary>
        /// FPrice
        /// </summary>
        public virtual decimal FPrice
        {
            get;
            set;
        }       

        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual c_set_type Unit
        {
            get;
            set;
        }

        public virtual string UnitName
        {
            get
            {
                return POTool.Str(() => { return Unit == null ? "" : Unit.FName; });
            }
        }
    }
}