﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_stonehouse
        public class c_stonehouse
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FNote
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FName
        /// </summary>
        public virtual string FName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FAddress
        /// </summary>
        public virtual string FAddress
        {
            get; 
            set; 
        }

        /// <summary>
        /// FManager
        /// </summary>
        public virtual int FManager
        {
            get;
            set;
        }      
        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual t_user Manager
        {
            get;
            set;
        }

        public virtual string ManagerName
        {
            get
            {
                return POTool.Str(() => { return Manager == null ? "" : Manager.FName; });
            }
        }
    }
}