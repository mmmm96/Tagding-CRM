﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_report_check
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 用户
        /// </summary>
        public virtual string FRealName
        {
            get;
            set;
        }
        /// <summary>
        /// 审批人
        /// </summary>
        public virtual int FApproverID
        {
            get;
            set;
        }
        /// <summary>
        /// 审批时间
        /// </summary>
        public virtual DateTime FApproverDate
        {
            get;
            set;
        }
        /// <summary>
        /// 原来数量
        /// </summary>
        public virtual int FOrgCount
        {
            get;
            set;
        }
        /// <summary>
        /// 实际数量
        /// </summary>
        public virtual int FAvailCount
        {
            get;
            set;
        }
        public virtual string FNote
        {
            get;
            set;
        }

        public virtual DateTime FCreateDate
        {
            get;
            set;
        }
        ////////////////////////////////扩展字段////////////////////////////////

    }
}