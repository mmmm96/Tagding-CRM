﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_service_problem
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 问题描述
        /// </summary>
        public virtual string FContent
        {
            get;
            set;
        }
        /// <summary>
        /// 提交日期
        /// </summary>
        public virtual DateTime FSubmitDate
        {
            get;
            set;
        }
        /// <summary>
        /// 解决日期
        /// </summary>
        public virtual DateTime FEndDate
        {
            get;
            set;
        }
        /// <summary>
        /// 客户
        /// </summary>
        public virtual int FCustID
        {
            get;
            set;
        }
        /// <summary>
        /// 问题解决情况
        /// </summary>
        public virtual string FReply
        {
            get;
            set;
        }
        /// <summary>
        /// 处理人
        /// </summary>
        public virtual int FUserID
        {
            get;
            set;
        }

        ////////////////////////////////扩展字段////////////////////////////////

    }
}