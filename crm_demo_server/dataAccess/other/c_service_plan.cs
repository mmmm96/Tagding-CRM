﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_service_plan
        public class c_service_plan
    {

          /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 计划名称
        /// </summary>
        public virtual string FTitle
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 计划内容
        /// </summary>
        public virtual string FContent
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 开始时间
        /// </summary>
        public virtual DateTime FBegin
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 结束时间
        /// </summary>
        public virtual DateTime FEnd
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 预算
        /// </summary>
        public virtual decimal FBudget
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 执行人
        /// </summary>
        public virtual int FUserID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 服务总结
        /// </summary>
        public virtual string FResult
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 所属合同
        /// </summary>
        public virtual int FContractID
        {
            get; 
            set; 
        }        
                 
        ////////////////////////////////扩展字段////////////////////////////////
         
    }
}