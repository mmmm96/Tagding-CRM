﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_reimbursement
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 报销内容
        /// </summary>
        public virtual string FContent
        {
            get;
            set;
        }
        /// <summary>
        /// 客户
        /// </summary>
        public virtual int FCustID
        {
            get;
            set;
        }
        /// <summary>
        /// 报销图片
        /// </summary>
        public virtual string FPic
        {
            get;
            set;
        }
        /// <summary>
        /// 报销人
        /// </summary>
        public virtual int FUserID
        {
            get;
            set;
        }

        ////////////////////////////////扩展字段////////////////////////////////

    }
}