﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //c_clue
        public class c_clue
    {

          /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 线索内容
        /// </summary>
        public virtual string FContent
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 预算
        /// </summary>
        public virtual decimal FBudget
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 联系人
        /// </summary>
        public virtual string FLinkMan
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 联系方式
        /// </summary>
        public virtual string FPhone
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 公司名称
        /// </summary>
        public virtual string FCompany
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 所属人
        /// </summary>
        public virtual int FUserID
        {
            get; 
            set; 
        }
        public virtual string FType
        {
            get;
            set;
        }        
        ////////////////////////////////扩展字段////////////////////////////////
         
    }
}