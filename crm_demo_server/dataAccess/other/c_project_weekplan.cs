﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_project_weekplan
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 标题
        /// </summary>
        public virtual string FTitle
        {
            get;
            set;
        }
        /// <summary>
        /// 内容
        /// </summary>
        public virtual string FContent
        {
            get;
            set;
        }
        /// <summary>
        /// 开始日期
        /// </summary>
        public virtual DateTime FBegDate
        {
            get;
            set;
        }
        /// <summary>
        /// 结束日期
        /// </summary>
        public virtual DateTime FEndDate
        {
            get;
            set;
        }
        /// <summary>
        /// 完成情况
        /// </summary>
        public virtual string FResult
        {
            get;
            set;
        }
        /// <summary>
        /// 计划人
        /// </summary>
        public virtual int FUserID
        {
            get;
            set;
        }

        ////////////////////////////////扩展字段////////////////////////////////

    }
}