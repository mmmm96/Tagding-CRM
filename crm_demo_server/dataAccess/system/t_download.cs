﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class t_download
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// FTitle
        /// </summary>
        public virtual string FTitle
        {
            get;
            set;
        }
        /// <summary>
        /// FPic
        /// </summary>
        public virtual string FPic
        {
            get;
            set;
        }
        /// <summary>
        /// FCreateDate
        /// </summary>
        public virtual DateTime FCreateDate
        {
            get;
            set;
        }

        public virtual string FNote
        {
            get;
            set;
        }

        public virtual string FType
        {
            get;
            set;
        }
        
        ////////////////////////////////扩展字段////////////////////////////////

    }
}