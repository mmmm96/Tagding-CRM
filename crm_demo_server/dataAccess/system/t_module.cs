﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //t_module
        public class t_module
    {

          /// <summary>
        /// 主键
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 模块名称
        /// </summary>
        public virtual string FName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 排序
        /// </summary>
        public virtual int FSort
        {
            get; 
            set; 
        }        
                 
        ////////////////////////////////扩展字段////////////////////////////////
         
    }
}