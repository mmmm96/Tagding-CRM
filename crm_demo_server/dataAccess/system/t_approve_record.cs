﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //t_approve_record
        public class t_approve_record
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FApproveID
        /// </summary>
        public virtual int FApproveID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FStep
        /// </summary>
        public virtual int FStep
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FStepName
        /// </summary>
        public virtual string FStepName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FApprover
        /// </summary>
        public virtual int FApprover
        {
            get; 
            set; 
        }
        /// <summary>
        /// FApprover
        /// </summary>
        public virtual int FToApprover
        {
            get;
            set;
        }      
        /// <summary>
        /// FApproverDate
        /// </summary>
        public virtual DateTime FApproverDate
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FApproverNote
        /// </summary>
        public virtual string FApproverNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FIsEnd
        /// </summary>
        public virtual int FIsEnd
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FBillID
        /// </summary>
        public virtual int FBillID
        {
            get; 
            set; 
        }
        /// <summary>
        /// FIsPass
        /// </summary>
        public virtual int FIsPass
        {
            get;
            set;
        }    
        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual t_user Approver
        {
            get;
            set;
        }

        public virtual string ApproverName
        {
            get
            {
                return POTool.Str(() => { return Approver == null ? "" : Approver.FName; });
            }
        }

        [ScriptIgnore]
        public virtual t_user ToApprover
        {
            get;
            set;
        }

        public virtual string ToApproverName
        {
            get
            {
                return POTool.Str(() => { return ToApprover == null ? "" : ToApprover.FName; });
            }
        }
    }
}