﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //t_dept_user
        public class t_dept_user
    {

          /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FUserID
        /// </summary>
        public virtual int FUserID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FDeptID
        /// </summary>
        public virtual int FDeptID
        {
            get; 
            set; 
        }

        public virtual string FUserName
        {
            get;
            set;
        }     

        ////////////////////////////////扩展字段////////////////////////////////
         
    }
}