﻿using System;
using System.Collections;
using System.Web.Script.Serialization;

namespace ReportFlat.DataAccess
{
   /// <summary>
   ///功能描述    :    
   ///开发者      :    
   ///建立时间    :    2013-6-8 19:45:40
   ///修订描述    :    
   ///进度描述    :    
   ///版本号      :    1.0
   ///最后修改时间:    2013-6-8 19:45:40
   ///
   ///Function Description :    
   ///Developer                :    
   ///Builded Date:    2013-6-8 19:45:40
   ///Revision Description :    
   ///Progress Description :    
   ///Version Number        :    1.0
   ///Last Modify Date     :    2013-6-8 19:45:40
   /// </summary>
      [Serializable]
   public class t_roledept
   {
      #region 构造函数
          public t_roledept()
      {}

      #endregion

      #region 成员
      private int mFID;
      private int mFRoleID;
      private int mFDeptID;
      #endregion


      #region 属性
      public  virtual int FID
      {
         get {  return mFID; }
         set {  mFID = value; }
      }

      public virtual int FDeptID
      {
          get { return mFDeptID; }
          set { mFDeptID = value; }
      }

      public  virtual int FRoleID
      {
         get {  return mFRoleID; }
         set {  mFRoleID = value; }
      }

  

      #endregion
      [ScriptIgnore]
      public virtual t_dept Dept
      {
          get;
          set;
      }

      public virtual string DeptName
      {
          get
          {
              return POTool.Str(() => { return Dept == null ? "" : Dept.FName; });
          }
      }
   }
}
