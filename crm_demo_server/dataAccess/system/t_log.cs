﻿using System;
using System.Collections;

namespace ReportFlat.DataAccess
{
   /// <summary>
   ///功能描述    :    
   ///开发者      :    
   ///建立时间    :    2014-1-24 17:49:27
   ///修订描述    :    
   ///进度描述    :    
   ///版本号      :    1.0
   ///最后修改时间:    2014-1-24 17:49:27
   ///
   ///Function Description :    
   ///Developer                :    
   ///Builded Date:    2014-1-24 17:49:27
   ///Revision Description :    
   ///Progress Description :    
   ///Version Number        :    1.0
   ///Last Modify Date     :    2014-1-24 17:49:27
   /// </summary>
   public class t_log
   {
      #region 构造函数
      public t_log()
      {}

      public t_log(int FID,string FOperator,string FTiTle,string FContent,int FUserID,string FUserName,DateTime FCreateDate,int FBillID)
      {
         this.mFID=FID;
         this.mFOperator=FOperator;
         this.mFTiTle=FTiTle;
         this.mFContent=FContent;
         this.mFUserID=FUserID;
         this.mFUserName=FUserName;
         this.mFCreateDate=FCreateDate;
         this.mFBillID=FBillID;
      }
      #endregion

      #region 成员
      private int mFID;
      private string mFOperator;
      private string mFTiTle;
      private string mFContent;
      private int mFUserID;
      private string mFUserName;
      private DateTime mFCreateDate;
      private int mFBillID;
      private string mFIp;
      #endregion


      #region 属性
      public  virtual int FID
      {
         get {  return mFID; }
         set {  mFID = value; }
      }

      public  virtual string FOperator
      {
         get {  return mFOperator; }
         set {  mFOperator = value; }
      }

      public  virtual string FTiTle
      {
         get {  return mFTiTle; }
         set {  mFTiTle = value; }
      }

      public  virtual string FContent
      {
         get {  return mFContent; }
         set {  mFContent = value; }
      }

      public  virtual int FUserID
      {
         get {  return mFUserID; }
         set {  mFUserID = value; }
      }

      public  virtual string FUserName
      {
         get {  return mFUserName; }
         set {  mFUserName = value; }
      }

      public  virtual DateTime FCreateDate
      {
         get {  return mFCreateDate; }
         set {  mFCreateDate = value; }
      }

      public  virtual int FBillID
      {
         get {  return mFBillID; }
         set {  mFBillID = value; }
      }

      public virtual string FIp
      {
          get { return mFIp; }
          set { mFIp = value; }
      }

      #endregion

   }
}
