﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //t_dept
        public class t_dept
    {

          /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }
        public virtual string FName
        {
            get;
            set;
        }   
        /// <summary>
        /// FParentID
        /// </summary>
        public virtual int FParentID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FSort
        /// </summary>
        public virtual int FSort
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FNote
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FDelFlag
        /// </summary>
        public virtual int FDelFlag
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FLeader
        /// </summary>
        public virtual string FLeader
        {
            get; 
            set; 
        }
        public virtual string FUpLeader
        {
            get;
            set;
        }    
        ////////////////////////////////扩展字段////////////////////////////////
         
    }
}