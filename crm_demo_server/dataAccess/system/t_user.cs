﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace ReportFlat.DataAccess
{
     [Serializable]
     public class t_user
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FCode
        /// </summary>
        public virtual string FCode
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FName
        /// </summary>
        public virtual string FName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FPassWord
        /// </summary>
       [ScriptIgnore]
        public virtual string FPassWord
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FNote
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FStatus
        /// </summary>
        public virtual string FStatus
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FPhone
        /// </summary>
        public virtual string FPhone
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FEMail
        /// </summary>
        public virtual string FEMail
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FType
        /// </summary>
        public virtual string FType
        {
            get; 
            set; 
        }

        public virtual string FAvatar
        {
            get;
            set;
        }

        public virtual string FRealName
        {
            get;
            set;
        }

        public virtual string FTelPhone
        {
            get;
            set;
        }

        [ScriptIgnore]
        public virtual IList<t_role> Roles
        {
            get;
            set;
        }
   }
}
