﻿using System;
using System.Collections;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class t_areas
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }

        public virtual int value
        {
            get
            {
                return FID;
            }
        }
        /// <summary>
        /// FAreaname
        /// </summary>
        public virtual string FName
        {
            get;
            set;
        }

        public virtual string label
        {
            get
            {
                return FName;
            }
        }

        /// <summary>
        /// FParentid
        /// </summary>
        public virtual int FParentid
        {
            get;
            set;
        }
        /// <summary>
        /// FShortname
        /// </summary>
        public virtual string FShortname
        {
            get;
            set;
        }
        /// <summary>
        /// FLng
        /// </summary>
        public virtual double FLng
        {
            get;
            set;
        }
        /// <summary>
        /// FLat
        /// </summary>
        public virtual double FLat
        {
            get;
            set;
        }
        /// <summary>
        /// FLevel
        /// </summary>
        public virtual int FLevel
        {
            get;
            set;
        }
        /// <summary>
        /// FPosition
        /// </summary>
        public virtual string FPosition
        {
            get;
            set;
        }
        /// <summary>
        /// FSort
        /// </summary>
        public virtual int FSort
        {
            get;
            set;
        }

    }
}