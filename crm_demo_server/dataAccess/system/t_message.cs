﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class t_message
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// FTitle
        /// </summary>
        public virtual string FTitle
        {
            get;
            set;
        }
        /// <summary>
        /// FSenderID
        /// </summary>
        public virtual int FSenderID
        {
            get;
            set;
        }
        /// <summary>
        /// FSenderName
        /// </summary>
        public virtual string FSenderName
        {
            get;
            set;
        }
        /// <summary>
        /// FDetail
        /// </summary>
        public virtual string FDetail
        {
            get;
            set;
        }
        /// <summary>
        /// FSendDate
        /// </summary>
        public virtual DateTime FSendDate
        {
            get;
            set;
        }
        /// <summary>
        /// FStatus
        /// </summary>
        public virtual int FStatus
        {
            get;
            set;
        }
        /// <summary>
        /// FReceiverID
        /// </summary>
        public virtual int FReceiverID
        {
            get;
            set;
        }
        /// <summary>
        /// FReceiverName
        /// </summary>
        public virtual string FReceiverName
        {
            get;
            set;
        }
        /// <summary>
        /// FBillType
        /// </summary>
        public virtual string FBillType
        {
            get;
            set;
        }
        /// <summary>
        /// FBillID
        /// </summary>
        public virtual int FBillID
        {
            get;
            set;
        }

        ////////////////////////////////扩展字段////////////////////////////////

    }
}