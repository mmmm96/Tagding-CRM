﻿using System;
using System.Collections;
using System.Web.Script.Serialization;

namespace ReportFlat.DataAccess
{
   /// <summary>
   ///功能描述    :    
   ///开发者      :    
   ///建立时间    :    2013-6-7 20:53:04
   ///修订描述    :    
   ///进度描述    :    
   ///版本号      :    1.0
   ///最后修改时间:    2013-6-7 20:53:04
   ///
   ///Function Description :    
   ///Developer                :    
   ///Builded Date:    2013-6-7 20:53:04
   ///Revision Description :    
   ///Progress Description :    
   ///Version Number        :    1.0
   ///Last Modify Date     :    2013-6-7 20:53:04
   /// </summary>
   public class t_role_aceess
   {
      #region 构造函数
      public t_role_aceess()
      {}

      public t_role_aceess(int FID,int FRoleID,int FMenuID,string FAccessName)
      {
         this.mFID=FID;
         this.mFRoleID=FRoleID;
         this.mFMenuID=FMenuID;
      }
      #endregion

      #region 成员
      private int mFID;
      private int mFRoleID;
      private int mFMenuID;
      #endregion


      #region 属性
      public  int FID
      {
         get {  return mFID; }
         set {  mFID = value; }
      }

      public  int FRoleID
      {
         get {  return mFRoleID; }
         set {  mFRoleID = value; }
      }

      public  int FMenuID
      {
         get {  return mFMenuID; }
         set {  mFMenuID = value; }
      }

      #endregion

   }
}
