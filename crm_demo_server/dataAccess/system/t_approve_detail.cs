﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
         //t_approve_detail
        public class t_approve_detail
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FApproveID
        /// </summary>
        public virtual int FApproveID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FStep
        /// </summary>
        public virtual int FStep
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FStepName
        /// </summary>
        public virtual string FStepName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FApprover
        /// </summary>
        public virtual string FApprover
        {
            get; 
            set; 
        }             
        /// <summary>
        /// FApproverField
        /// </summary>
        public virtual string FApproverField
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FDatetimeField
        /// </summary>
        public virtual string FDatetimeField
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FIsEnd
        /// </summary>
        public virtual int FIsEnd
        {
            get; 
            set; 
        }
        /// <summary>
        /// FPassVal
        /// </summary>
        public virtual int FPassVal
        {
            get;
            set;
        }
        /// <summary>
        /// FBackVal
        /// </summary>
        public virtual int FBackVal
        {
            get;
            set;
        }     
                 
        ////////////////////////////////扩展字段////////////////////////////////
         
    }
}