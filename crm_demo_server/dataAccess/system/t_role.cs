﻿using System;
using System.Collections;

namespace ReportFlat.DataAccess
{
   /// <summary>
   ///功能描述    :    
   ///开发者      :    
   ///建立时间    :    2013-6-7 20:53:03
   ///修订描述    :    
   ///进度描述    :    
   ///版本号      :    1.0
   ///最后修改时间:    2013-6-7 20:53:03
   ///
   ///Function Description :    
   ///Developer                :    
   ///Builded Date:    2013-6-7 20:53:03
   ///Revision Description :    
   ///Progress Description :    
   ///Version Number        :    1.0
   ///Last Modify Date     :    2013-6-7 20:53:03
   /// </summary>
   public class t_role
   {
      #region 构造函数
      public t_role()
      {}

      public t_role(int FID,string FName,string FNote)
      {
         this.mFID=FID;
         this.mFName=FName;
         this.mFNote=FNote;
      }
      #endregion

      #region 成员
      private int mFID;
      private string mFName;
      private string mFNote;
      #endregion


      #region 属性
      public  int FID
      {
         get {  return mFID; }
         set {  mFID = value; }
      }

      public  string FName
      {
         get {  return mFName; }
         set {  mFName = value; }
      }

      public  string FNote
      {
         get {  return mFNote; }
         set {  mFNote = value; }
      }

      #endregion

   }
}
