﻿using System;
using System.Collections;

namespace ReportFlat.DataAccess
{
   /// <summary>
   ///功能描述    :    
   ///开发者      :    
   ///建立时间    :    2013-6-9 18:58:38
   ///修订描述    :    
   ///进度描述    :    
   ///版本号      :    1.0
   ///最后修改时间:    2013-6-9 18:58:38
   ///
   ///Function Description :    
   ///Developer                :    
   ///Builded Date:    2013-6-9 18:58:38
   ///Revision Description :    
   ///Progress Description :    
   ///Version Number        :    1.0
   ///Last Modify Date     :    2013-6-9 18:58:38
   /// </summary>
   public class t_system
   {
      #region 构造函数
      public t_system()
      {}

      public t_system(int FID,string FKey,string FValue,int FType,string FNote)
      {
         this.mFID=FID;
         this.mFKey=FKey;
         this.mFValue=FValue;
         this.mFType=FType;
         this.mFNote=FNote;
      }
      #endregion

      #region 成员
      private int mFID;
      private string mFKey;
      private string mFValue;
      private int mFType;
      private string mFNote;
      #endregion


      #region 属性
      public  virtual int FID
      {
         get {  return mFID; }
         set {  mFID = value; }
      }

      public  virtual string FKey
      {
         get {  return mFKey; }
         set {  mFKey = value; }
      }

      public  virtual string FValue
      {
         get {  return mFValue; }
         set {  mFValue = value; }
      }

      public  virtual int FType
      {
         get {  return mFType; }
         set {  mFType = value; }
      }

      public  virtual string FNote
      {
         get {  return mFNote; }
         set {  mFNote = value; }
      }

      public virtual int FSys
      {
          get;
          set;
      }

      #endregion

   }
}
