﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Serialization;


//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
        public class t_menu
    {

          /// <summary>
        /// auto_increment
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FName
        /// </summary>
        public virtual string FName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FOrder
        /// </summary>
        public virtual int FOrder
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FParent
        /// </summary>
        public virtual int FParent
        {
            get; 
            set; 
        }        
   
        /// <summary>
        /// FCode
        /// </summary>
        public virtual string FCode
        {
            get; 
            set; 
        }        
       
        /// <summary>
        /// FIconCls_Vue
        /// </summary>
        public virtual string FIconCls_Vue
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FPath_Vue
        /// </summary>
        public virtual string FUrl_Vue
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FRequire_Vue
        /// </summary>
        public virtual string FRequire_Vue
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FVisable_Vue
        /// </summary>
        public virtual Boolean FVisable_Vue
        {
            get; 
            set; 
        }
        /// <summary>
        /// FParent
        /// </summary>
        public virtual int FLevel
        {
            get;
            set;
        }
        public virtual string FQuery
        {
            get;
            set;
        }

        /// <summary>
        /// FModuleID
        /// </summary>
        public virtual int FModuleID
        {
            get;
            set;
        }
   }
}
