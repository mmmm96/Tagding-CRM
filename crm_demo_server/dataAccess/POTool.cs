﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReportFlat.DataAccess
{
    public delegate string Running();
    class POTool
    {
        //延时加载时，关联关系使用的是Load,没有关联数据时，会抛出异常
        public static string Str(Running run)
        {
            try
            {
               return run();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
    }
}
