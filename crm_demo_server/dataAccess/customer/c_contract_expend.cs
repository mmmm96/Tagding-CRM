using System;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
        public class c_contract_expend
    {

          /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 费用类型
        /// </summary>
        public virtual string FExpendType
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 金额
        /// </summary>
        public virtual decimal FAmount
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime FCreateDate
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }
        public virtual string FUserName
        {
            get;
            set;
        }
        /// <summary>
        /// 合同ID
        /// </summary>
        public virtual int FContractID
        {
            get;
            set;
        }
    }
}