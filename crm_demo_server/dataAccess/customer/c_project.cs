﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_project
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 客户ID
        /// </summary>
        public virtual int FCustID
        {
            get;
            set;
        }
        /// <summary>
        /// 项目名称
        /// </summary>
        public virtual string FName
        {
            get;
            set;
        }
        /// <summary>
        /// 项目建设内容
        /// </summary>
        public virtual string FContent
        {
            get;
            set;
        }
        /// <summary>
        /// 建设期限begin
        /// </summary>
        public virtual DateTime FDeadlineBeg
        {
            get;
            set;
        }
        /// <summary>
        /// 建设期限end
        /// </summary>
        public virtual DateTime FDeadlineEnd
        {
            get;
            set;
        }
        /// <summary>
        /// 总投资额
        /// </summary>
        public virtual decimal FAmount
        {
            get;
            set;
        }
        /// <summary>
        /// 固定投资
        /// </summary>
        public virtual decimal FFixAmount
        {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime FCreateDate
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get;
            set;
        }
        /// <summary>
        /// 项目地址
        /// </summary>
        public virtual string FAddress
        {
            get;
            set;
        }
        /// <summary>
        /// 是否获取资金
        /// </summary>
        public virtual string FIsBankroll
        {
            get;
            set;
        }
        /// <summary>
        /// 是否签合同
        /// </summary>
        public virtual string FIsHasContract
        {
            get;
            set;
        }
        /// <summary>
        /// 项目进度
        /// </summary>
        public virtual string FStatus
        {
            get;
            set;
        }
        /// <summary>
        /// 项目来源
        /// </summary>
        public virtual string FSource
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_1
        /// </summary>
        public virtual Boolean FCollection_1
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_2
        /// </summary>
        public virtual Boolean FCollection_2
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_3
        /// </summary>
        public virtual Boolean FCollection_3
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_4
        /// </summary>
        public virtual Boolean FCollection_4
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_5
        /// </summary>
        public virtual Boolean FCollection_5
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_6
        /// </summary>
        public virtual Boolean FCollection_6
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_7
        /// </summary>
        public virtual Boolean FCollection_7
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_8
        /// </summary>
        public virtual Boolean FCollection_8
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_9
        /// </summary>
        public virtual Boolean FCollection_9
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_10
        /// </summary>
        public virtual Boolean FCollection_10
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_11
        /// </summary>
        public virtual Boolean FCollection_11
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_12
        /// </summary>
        public virtual Boolean FCollection_12
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_13
        /// </summary>
        public virtual Boolean FCollection_13
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_14
        /// </summary>
        public virtual Boolean FCollection_14
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_15
        /// </summary>
        public virtual Boolean FCollection_15
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_16
        /// </summary>
        public virtual Boolean FCollection_16
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_17
        /// </summary>
        public virtual Boolean FCollection_17
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_18
        /// </summary>
        public virtual Boolean FCollection_18
        {
            get;
            set;
        }
        /// <summary>
        /// FCollection_19
        /// </summary>
        public virtual string FCollection_19
        {
            get;
            set;
        }        

        /// <summary>
        //////////////////////////// 扩展字段 //////////////////////////////
        /// </summary>
        public virtual string FDeadline
        {
            get
            {
                string result = "";
                if (FDeadlineBeg!=null)
                {
                    result += FDeadlineBeg.ToString("yyyy.MM");
                }
                result += "---";
                if (FDeadlineEnd != null)
                {
                    result += FDeadlineEnd.ToString("yyyy.MM");
                }
                return result;
            }
        }

        public virtual string Collections
        {
            get
            {
                string r = "";
                r += FCollection_1?"1、项目可行性研究报告或项目建议书<br>":"";
                r += FCollection_2 ? "2、营业执照<br>" : "";
                r += FCollection_3 ? "3、项目备案<br>" : "";
                r += FCollection_4 ? "4、环境影响报告批复或环境影响报告表<br>" : "";
                r += FCollection_5 ? "5、节能评估报告批复或节能登记证<br>" : "";
                r += FCollection_6 ? "6、土地建设规划批复<br>" : "";
                r += FCollection_7 ? "7、用地批复或土地证<br>" : "";
                r += FCollection_8 ? "8、银行存款证明<br>" : "";
                r += FCollection_9 ? "9、贷款意向书或贷款合同<br>" : "";
                r += FCollection_10 ? "10、上年度财务审计报告<br>" : "";
                r += FCollection_11 ? "11、本年财务报表<br>" : "";
                r += FCollection_12 ? "12、生产许可证<br>" : "";
                r += FCollection_13 ? "13、施工合同或发票<br>" : "";
                r += FCollection_14 ? "14、设备等材料采购合同或发票<br>" : "";
                r += FCollection_15 ? "15、施工照片或厂区现场照片<br>" : "";
                r += FCollection_16 ? "16、企业基本情况表<br>" : "";
                r += FCollection_17 ? "17、国税纳税情况<br>" : "";
                r += FCollection_18 ? "18、地税纳税情况<br>" : "";
                r += string.IsNullOrEmpty(FCollection_19) ?"": "19、"+FCollection_19;
                return r;
            }
        }
    }
}