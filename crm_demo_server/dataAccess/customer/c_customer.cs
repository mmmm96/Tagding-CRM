﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
        public class c_customer
    {

          /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 客户名称
        /// </summary>
        public virtual string FName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 客户类别
        /// </summary>
        public virtual string FGrade
        {
            get; 
            set; 
        }
        /// <summary>
        /// 意向客户类别
        /// </summary>
        public virtual string FGradeYX
        {
            get;
            set;
        }    
        /// <summary>
        /// 所属行业
        /// </summary>
        public virtual string FIndustry
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 省份ID
        /// </summary>
        public virtual int FProviceID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 省份
        /// </summary>
        public virtual string FProvice
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 城市ID
        /// </summary>
        public virtual int FCityID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 城市
        /// </summary>
        public virtual string FCity
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 地址
        /// </summary>
        public virtual string FAddress
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 是否万家企业
        /// </summary>
        public virtual string F10000
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 是否高耗能企业
        /// </summary>
        public virtual string FHeightEnergy
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FUserID
        /// </summary>
        public virtual int FUserID
        {
            get; 
            set; 
        }
        /// <summary>
        /// 原业务员ID
        /// </summary>
        public virtual int FOrgUserID
        {
            get;
            set;
        }
        
        /// <summary>
        /// 法人
        /// </summary>
        public virtual string FCorporation
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FCreateDate
        /// </summary>
        public virtual DateTime FCreateDate
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FContactsInfo
        /// </summary>
        public virtual string FContactsInfo
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FProjectInfo
        /// </summary>
        public virtual string FProjectInfo
        {
            get; 
            set; 
        }
        /// <summary>
        /// FTraceInfo
        /// </summary>
        public virtual string FTraceInfo
        {
            get;
            set;
        }
        /// <summary>
        /// FDesc
        /// </summary>
        public virtual string FDesc
        {
            get;
            set;
        }
        /// <summary>
        /// FGrade_1
        /// </summary>
        public virtual string FGrade_1
        {
            get;
            set;
        }
        /// <summary>
        /// FGrade_2
        /// </summary>
        public virtual string FGrade_2
        {
            get;
            set;
        }
        /// <summary>
        /// FGrade_3
        /// </summary>
        public virtual string FGrade_3
        {
            get;
            set;
        }   

        ////////////////////////////////扩展字段////////////////////////////////
        [ScriptIgnore]
        public virtual t_user User
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get
            {
                return POTool.Str(() => { return User == null ? "" : User.FName; });
            }
        }

    }
}