﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_project_trace
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 项目ID
        /// </summary>
        public virtual int FProjectID
        {
            get;
            set;
        }
        /// <summary>
        /// 标题
        /// </summary>
        public virtual string FTitle
        {
            get;
            set;
        }
        /// <summary>
        /// 跟进时间
        /// </summary>
        public virtual DateTime FCreateDate
        {
            get;
            set;
        }

        /// <summary>
        /// 再跟进时间
        /// </summary>
        public virtual DateTime FToDate
        {
            get;
            set;
        }

        /// <summary>
        /// 跟进内容
        /// </summary>
        public virtual string FContent
        {
            get;
            set;
        }
        /// <summary>
        /// 客方联系人
        /// </summary>
        public virtual string FLinkMan
        {
            get;
            set;
        }
        /// <summary>
        /// 客方联系电话
        /// </summary>
        public virtual string FLinkPhone
        {
            get;
            set;
        }
        /// <summary>
        /// 业务员
        /// </summary>
        public virtual int FUserID
        {
            get;
            set;
        }
        /// <summary>
        /// 客户ID
        /// </summary>
        public virtual int FCustID
        {
            get;
            set;
        }

        public virtual int FType
        {
            get;
            set;
        }

        public virtual int FOrgUserID
        {
            get;
            set;
        }

        /// <summary>
        /// 真实名字
        /// </summary>
        public virtual string FRealName
        {
            get;
            set;
        }

        public virtual string FMyPhone
        {
            get;
            set;
        }

        public virtual int FTalkTime
        {
            get;
            set;
        }
        ////////////////////////////////扩展字段////////////////////////////////

        
        [ScriptIgnore]
        public virtual c_project Project
        {
            get;
            set;
        }

        public virtual string ProjectName
        {
            get
            {
                return POTool.Str(() => { return Project == null ? "" : Project.FName; });
            }
        }


        [ScriptIgnore]
        public virtual c_customer Customer
        {
            get;
            set;
        }

        public virtual string CustomerName
        {
            get
            {
                return POTool.Str(() => { return Customer == null ? "" : Customer.FName; });
            }
        }



        [ScriptIgnore]
        public virtual t_user User
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get
            {
                return POTool.Str(() => { return User == null ? "" : User.FName; });
            }
        }

        public virtual string UserRealName
        {
            get
            {
                return POTool.Str(() => { return User == null ? "" : User.FRealName; });
            }
        }
    }
}