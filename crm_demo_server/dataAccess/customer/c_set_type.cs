using System;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
        public class c_set_type
    {

          /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string FName
        {
            get; 
            set; 
        }        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }
        /// <summary>
        /// 1、资金类型 2、支出费用类型
        /// </summary>
        public virtual int FType
        {
            get;
            set;
        }     
    }
}