using System;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
        public class c_project_bankroll
    {

          /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get; 
            set; 
        }
        public virtual int FProjectID
        {
            get;
            set;
        }    
        /// <summary>
        /// FBankrollType
        /// </summary>
        public virtual string FBankrollType
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FDeadlineBeg
        /// </summary>
        public virtual DateTime FDeadlineBeg
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FDeadlineEnd
        /// </summary>
        public virtual DateTime FDeadlineEnd
        {
            get; 
            set; 
        }

        /// <summary>
        /// FAmount
        /// </summary>
        public virtual decimal FAmount
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FBankrollGrade
        /// </summary>
        public virtual string FBankrollGrade
        {
            get; 
            set; 
        }        
        /// <summary>
        /// FNote
        /// </summary>
        public virtual string FNote
        {
            get; 
            set; 
        }
        public virtual int FCustID
        {
            get;
            set;
        }

        /// <summary>
        ////////////////////////////////// ��չ�ֶ� /////////////////////////////////////
        /// </summary>
        public virtual string FDeadline
        {
            get
            {
                string result = "";
                if (FDeadlineBeg != null)
                {
                    result += FDeadlineBeg.ToString("yyyy");
                }
                result += "---";
                if (FDeadlineEnd != null)
                {
                    result += FDeadlineEnd.ToString("yyyy");
                }
                return result;
            }
        }
      
        
    }
}