﻿using System;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_contacts
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        public virtual int FCustID
        {
            get;
            set;
        }
        
        /// <summary>
        /// 姓名
        /// </summary>
        public virtual string FName
        {
            get;
            set;
        }
        /// <summary>
        /// 职位
        /// </summary>
        public virtual string FPosition
        {
            get;
            set;
        }
        /// <summary>
        /// 手机
        /// </summary>
        public virtual string FPhone
        {
            get;
            set;
        }
        /// <summary>
        /// 固定电话
        /// </summary>
        public virtual string FTelphone
        {
            get;
            set;
        }
        /// <summary>
        /// 邮箱
        /// </summary>
        public virtual string FEmail
        {
            get;
            set;
        }
        /// <summary>
        /// QQ
        /// </summary>
        public virtual string FQQ
        {
            get;
            set;
        }
        /// <summary>
        /// 传真
        /// </summary>
        public virtual string FFax
        {
            get;
            set;
        }
        /// <summary>
        /// 微信
        /// </summary>
        public virtual string FWx
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get;
            set;
        }
        /// <summary>
        /// FCreateDate
        /// </summary>
        public virtual DateTime FCreateDate
        {
            get;
            set;
        }
        /// <summary>
        /// 性别
        /// </summary>
        public virtual string FSex
        {
            get;
            set;
        }
        /// <summary>
        /// 年龄
        /// </summary>
        public virtual int FAge
        {
            get;
            set;
        }
        /// <summary>
        /// 籍贯
        /// </summary>
        public virtual string FOriginPlace
        {
            get;
            set;
        }
        /// <summary>
        /// 部门
        /// </summary>
        public virtual string FDept
        {
            get;
            set;
        }
        /// <summary>
        /// 负责项目
        /// </summary>
        public virtual string FForProject
        {
            get;
            set;
        }

    }
}