using System;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_contract_income
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 合同ID
        /// </summary>
        public virtual int FContractID
        {
            get;
            set;
        }
        /// <summary>
        /// 费用类型
        /// </summary>
        public virtual string FIncomeType
        {
            get;
            set;
        }
        /// <summary>
        /// 付款金额
        /// </summary>
        public virtual decimal FPayAmount
        {
            get;
            set;
        }
        /// <summary>
        /// 付款时间
        /// </summary>
        public virtual DateTime FPayDate
        {
            get;
            set;
        }
        /// <summary>
        /// 开票金额
        /// </summary>
        public virtual decimal FInvoiceAmount
        {
            get;
            set;
        }
        /// <summary>
        /// 开票时间
        /// </summary>
        public virtual DateTime FVoiceDate
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get;
            set;
        }
        /// <summary>
        /// 经办人
        /// </summary>
        public virtual string FUserName
        {
            get;
            set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public virtual DateTime FCreateDate
        {
            get;
            set;
        }

        ////////////////////////////////扩展字段////////////////////////////////

    }
}