﻿using System;
using System.Web.Script.Serialization;

//Nhibernate Code Generation Template 1.0
//author:MythXin
//blog:www.cnblogs.com/MythXin
//Entity Code Generation Template
namespace ReportFlat.DataAccess
{
    public class c_contract
    {

        /// <summary>
        /// FID
        /// </summary>
        public virtual int FID
        {
            get;
            set;
        }
        /// <summary>
        /// 合同编号
        /// </summary>
        public virtual string FNo
        {
            get;
            set;
        }
        /// <summary>
        /// 公司名称
        /// </summary>
        public virtual int FCustID
        {
            get;
            set;
        }
        /// <summary>
        /// 工程ID
        /// </summary>
        public virtual int FProjectID
        {
            get;
            set;
        }
        /// <summary>
        /// 项目名称
        /// </summary>
        public virtual string FProjectName
        {
            get;
            set;
        }
        /// <summary>
        /// 申报等级
        /// </summary>
        public virtual string FGrade
        {
            get;
            set;
        }
        /// <summary>
        /// 申报资金种类
        /// </summary>
        public virtual string FBankrollType
        {
            get;
            set;
        }
        /// <summary>
        /// 建设期限begin
        /// </summary>
        public virtual DateTime FDeadlineBeg
        {
            get;
            set;
        }
        /// <summary>
        /// 建设期限end
        /// </summary>
        public virtual DateTime FDeadlineEnd
        {
            get;
            set;
        }
        /// <summary>
        /// 总金额
        /// </summary>
        public virtual decimal FAmount
        {
            get;
            set;
        }

        /// <summary>
        /// 客户获得金额总额
        /// </summary>
        public virtual decimal FActAmount
        {
            get;
            set;
        }

        /// <summary>
        /// 已付金额
        /// </summary>
        public virtual decimal FPayedAmount
        {
            get;
            set;
        }
        /// <summary>
        /// 付款方式:分期、一次性
        /// </summary>
        public virtual string FPayWay
        {
            get;
            set;
        }
        /// <summary>
        /// 合同点数
        /// </summary>
        public virtual decimal FCustCommission
        {
            get;
            set;
        }
        /// <summary>
        /// 业务提成
        /// </summary>
        public virtual string FCommission
        {
            get;
            set;
        }
        /// <summary>
        /// 合同完成时间
        /// </summary>
        public virtual DateTime FToCompletedDate
        {
            get;
            set;
        }
        /// <summary>
        /// 公司规定完成时间
        /// </summary>
        public virtual DateTime FCompletedDate
        {
            get;
            set;
        }
        /// <summary>
        /// 咨询师
        /// </summary>
        public virtual string FCounselor
        {
            get;
            set;
        }
        /// <summary>
        /// 更换咨询师
        /// </summary>
        public virtual string FSecCounselor
        {
            get;
            set;
        }
        /// <summary>
        /// 业务员ID
        /// </summary>
        public virtual int FUserID
        {
            get;
            set;
        }

        /// <summary>
        /// 原业务员ID
        /// </summary>
        public virtual int FOrgUserID
        {
            get;
            set;
        }
        
        /// <summary>
        /// 所属部门
        /// </summary>
        public virtual string FDeptName
        {
            get;
            set;
        }
        /// <summary>
        /// FCreateDate
        /// </summary>
        public virtual DateTime FCreateDate
        {
            get;
            set;
        }
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string FNote
        {
            get;
            set;
        }
        /// <summary>
        /// 收入信息
        /// </summary>
        public virtual string FIncomeInfo
        {
            get;
            set;
        }
        /// <summary>
        /// 费用信息
        /// </summary>
        public virtual string FExpendInfo
        {
            get;
            set;
        }  

        ///////////////////////// 扩展字段 /////////////////////////

        public virtual string FDeadline
        {
            get
            {
                string result = "";
                if (FDeadlineBeg != null)
                {
                    result += FDeadlineBeg.ToString("yyyy");
                }
                result += "---";
                if (FDeadlineEnd != null)
                {
                    result += FDeadlineEnd.ToString("yyyy");
                }
                return result;
            }
        }

        public virtual decimal ToAmount
        {
            get
            {
                return FAmount * FCustCommission / 100;
            }
        }

        [ScriptIgnore]
        public virtual c_project Project
        {
            get;
            set;
        }

        public virtual string Collections
        {
            get
            {
                return POTool.Str(() => { return Project == null ? "" : Project.Collections; });
            }
        }

        public virtual string IsBankroll
        {
            get
            {
                if (ToAmount > 0)
                {
                    return "是";
                }
                else
                {
                    return "否";
                }
            }
        }

        /// <summary>
        /// sy_produce_plan
        /// </summary>

        [ScriptIgnore]
        public virtual c_customer Customer
        {
            get;
            set;
        }

        public virtual string CustomerName
        {
            get
            {
                return POTool.Str(() => { return Customer == null ? "" : Customer.FName; });
            }
        }

        public virtual string Provice
        {
            get
            {
                return POTool.Str(() => { return Customer == null ? "" : Customer.FProvice; });
            }
        }

        public virtual string City
        {
            get
            {
                return POTool.Str(() => { return Customer == null ? "" : Customer.FCity; });
            }
        }

        //FPayedAll
        public virtual string PayedAll
        {
            get
            {
                if (FActAmount <= FPayedAmount)
                {
                    return "否";
                }else{
                    return "是";
                }
            }
        }
        [ScriptIgnore]
        public virtual t_user User
        {
            get;
            set;
        }

        public virtual string UserName
        {
            get
            {
                return POTool.Str(() => { return User == null ? "" : User.FName; });
            }
        }
    }
}