﻿using ReportFlat.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;
using System.Web;

namespace ReportFlat.Web
{
    public class AutoTask
    {
        //单列模式，静态变量，可长时间贮存在内存
        private static AutoTask _instance = null;
        private Timer timer = new Timer();

        private AutoTask() { }
        public static AutoTask CreateInstance()
        {
            if (_instance == null)
            {
                _instance = new AutoTask();
            }
            return _instance;
        }

        //定时器
        public void RunTimer()
        {
            timer.Enabled = true;
            timer.Interval = 10 * 60 * 1000;//执行间隔时间,单位为毫秒  10分钟执行一次
            timer.Start();
            timer.Elapsed += new ElapsedEventHandler(this.AutoRequestPage);
        }

        /// <summary>
        /// 重新访问,防止程序池回收
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoRequestPage(object sender, System.Timers.ElapsedEventArgs e)
        {
            string url = Const.WebHost;
            System.Net.HttpWebRequest myHttpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
            System.Net.HttpWebResponse myHttpWebResponse = (System.Net.HttpWebResponse)myHttpWebRequest.GetResponse();
            Stream receiveStream = myHttpWebResponse.GetResponseStream();//得到回写的字节流 
        }
    }
}