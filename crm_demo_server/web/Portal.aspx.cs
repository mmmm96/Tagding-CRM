﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ReportFlat.bussiness;
using ReportFlat.DataAccess;
using ReportFlat.Common;
using System.Collections.Generic;

namespace ReportFlat.Web
{
    public partial class Portal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cmd = base.Request.Params["cmd"];
            if (cmd == null)
            {
                Response.Redirect("client/main/login.html");
                return;
            }
            if (cmd == "login")
            {
                login();
            }
            else if(cmd == "Logout"){
                Logout();
            }
            else if (cmd == "Ticket")
            {
                //心跳包
                Response.Write(JSONHelper.SuccessJson);
            }
        }

        //供测试使用
        private void loginTest()
        {
            string userName = Request.Params["userName"];
            t_user user = BusTool.DBGet<t_user>("FCode = '" + userName + "'");
            if (user == null)
            {
                Response.Write(JSONHelper.ToErroJson("没有该用户！"));
                return;
            }
            //登录成功
            Session["login"] = new MLogin(user);
            Response.Write(JSONHelper.SuccessJson);
        }

        public void login()
        {
            //end of------测试使用,发布时要记得删除掉
            string userName = Request.Params["userName"];
            string password = Request.Params["password"];
            t_user user = BusTool.DBGet<t_user>("FCode = '" + userName + "'");
            if (user == null)
            {
                Response.Write(JSONHelper.ToErroJson("没有该用户！"));
                return;
            }
            //检测密码
            if (!password.ToLower().Equals(user.FPassWord.ToLower()))
            {
                Response.Write(JSONHelper.ToErroJson("密码错误！"));
                return;
            }
            //登录成功
            MLogin GMLogin = new MLogin(user);
            Session["login"] = GMLogin;
            //记录日志
            t_log log = new t_log();
            log.FContent = "登录成功：" + userName;
            log.FCreateDate = DateTime.Now;
            log.FOperator = "登录";
            log.FTiTle = "登录";
            log.FUserName = userName;
            log.FUserID = user.FID;
            log.FIp = Request.ServerVariables.Get("Remote_Addr");
            BusLog.Save(log);
            Response.Write(JSONHelper.AddF("user", user)
                                    .AddF("roles", GetRoles(GMLogin))
                                    .AddF("authority", GetAuthority(GMLogin))
                                    .ToSucJson()
                );
        }

        private IList<int> GetRoles(MLogin GMLogin)
        {
            if (GMLogin == null)
            {
                return null;
            }
            IList<int> result = new List<int>();
            if (GMLogin.isAdminUser())
            {
                result.Add(-1);
                return result;
            }
            if (GMLogin.User != null && GMLogin.User.Roles != null && GMLogin.User.Roles.Count > 0)
            {
                foreach (t_role role in GMLogin.User.Roles)
                {
                    //to-do:属于多个role时，去重
                    result.Add(role.FID);
                }
            }
            return result;
        }

        private IList<int> GetAuthority(MLogin GMLogin)
        {
            if (GMLogin == null)
            {
                return null;
            }
            IList<int> result = new List<int>();
            if (GMLogin.isAdminUser())
            {
                result.Add(-1);
                return result;
            }
            if (GMLogin.User != null && GMLogin.User.Roles != null && GMLogin.User.Roles.Count > 0)
            {
                foreach (t_role role in GMLogin.User.Roles)
                {
                    foreach (t_role_aceess role_aceess in CacheManager.instance.GetRoleAccess(role.FID))
                    {
                        //to-do:属于多个role时，去重
                        result.Add(role_aceess.FMenuID);
                    }
                }
            }
            return result;
        }

        public void Logout()
        {
            MLogin mlogin = (MLogin)Session["login"];
            if (mlogin != null)
            {
                //记录日志
                t_log log = new t_log();
                log.FContent = "注销成功：" + mlogin.User.FName;
                log.FCreateDate = DateTime.Now;
                log.FOperator = "注销";
                log.FTiTle = "注销";
                log.FUserName = mlogin.User.FName;
                log.FUserID = mlogin.UserID;
                BusLog.Save(log);
                Session["login"] = null;
                Response.Redirect("Default.aspx");
            }
        }
    }
}
