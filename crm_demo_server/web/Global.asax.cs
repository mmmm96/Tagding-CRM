﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Configuration;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using ReportFlat.bussiness;

namespace ReportFlat.Web
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            Const.WebHost = ConfigurationSettings.AppSettings["WebHost"];
            Const.WebPath = HttpRuntime.AppDomainAppPath;
            Const.SqlConn = ConfigurationSettings.AppSettings["SqlConn"];
            //启动数据库
            HibernateSessionFactory.Init();
            //缓存
            //CacheManager.instance.Start();
            //运行任务
            AutoTask.CreateInstance().RunTimer(); 
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}