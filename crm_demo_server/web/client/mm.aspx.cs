﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReportFlat.Common;

namespace ReportFlat.Web.client.system
{
    public partial class DPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["psw"]!="tDJHSO4HoN0qSI03AT")
            {
                //Response.Redirect("../main/login.html");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            tbOutput.Text = Encryption.AESEncrypt(tbInput.Text);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            tbOutput.Text = Encryption.AESDecrypt(tbInput.Text);
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            tbOutput.Text = Encryption.HJMD5(tbInput.Text);
        }
    }
}