﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Reflection;
using System.ComponentModel;
using ReportFlat.bussiness;
using ReportFlat.Common;

namespace ReportFlat.Web
{
    public class BaseClientPage : System.Web.UI.Page
    {
        public int MenuID;

        public MLogin GMLogin
        {
            get
            {
                if (Session["login"] != null)
                {
                    return (MLogin)Session["login"];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                Session["login"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //测试用
            //login();

            if (Request.Params["MenuID"] != null)
            {
                MenuID = Convert.ToInt32(Request.Params["MenuID"]);
            }

            if (Session["login"] == null)
            {
                Response.Redirect("../main/login.html");
                //模拟登陆
                //login();
            }
            else
            {
                MLogin mLogin = (MLogin)Session["login"];
                int userID = mLogin.User.FID;
                string userName = mLogin.User.FName;
                int roleID = 0;
                if (mLogin.User.FID==-1)
                {
                    roleID = -1;
                }
                else if (mLogin.User.Roles != null && mLogin.User.Roles.Count > 0)
                {
                    roleID = mLogin.User.Roles[0].FID;
                }
                string jsVariant = @"<script type='text/javascript'> 
                                var g_userID = {0} ; 
                                var g_userName = '{1}' ;
                                var g_roleID = {2};
                                var g_userAccess = '1111111111';
                                var g_appPath ='{3}';
                                var g_menuID ={4};
                                var g_userType = '{5}';
                                </script>";
                string appPath = Request.ApplicationPath;
                if (appPath.EndsWith("/"))
                {
                    appPath = appPath.Remove(appPath.Length - 1);
                }
                Response.Write(String.Format(jsVariant, userID, userName, roleID, appPath,
                    MenuID, mLogin.User.FType));
            }
        }
    }
}
