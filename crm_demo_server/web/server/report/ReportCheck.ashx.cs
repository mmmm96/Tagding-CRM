﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;

namespace ReportFlat.Web.server.report
{
    /// <summary>
    /// ReportCheck 的摘要说明
    /// </summary>
    public class ReportCheck : BaseHandler
    {

        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_report_check obj = ToPo<c_report_check>();
            //
            c_report_check obj2 = dao.Get<c_report_check>("FRealName=" + obj.FRealName.wrap() + " and FCreateDate=" + obj.FCreateDate.ToString("yyyy-MM-dd").wrap());
            if (obj2 == null)
            {
                obj.FApproverID = GMLogin.UserID;
                obj.FApproverDate = DateTime.Now.Date;
                dao.Save(obj);
            }
            else
            {
                obj2.FAvailCount = obj.FAvailCount;
                obj2.FNote = obj.FNote;
                dao.Update(obj2);
            }
            jsonResult = JSONHelper.SuccessJson;
        }
    }
}