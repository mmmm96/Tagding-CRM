﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.report
{
    public class ReportTrace : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_report_trace obj = ToPo<c_report_trace>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.Delete(new c_report_trace() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            //11是总经理
            if (!GMLogin.isAdminUser())
            {
                sWhere = sWhere.and("FUserID=" + GMLogin.UserID);
            }
            int rowCount;
            IList<c_report_trace> list = dao.GetList<c_report_trace>(out rowCount, sWhere, "FCreateDate desc", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_report_trace> list = dao.GetList<c_report_trace>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}