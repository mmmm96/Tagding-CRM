﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.customer
{
    /// <summary>
    /// Supplier 的摘要说明
    /// </summary>
    public class ContractIncome : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_contract_income obj = ToPo<c_contract_income>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            WriteBackContract(obj.FContractID);
            jsonResult = JSONHelper.SuccessJson;
        }

        //回写合同已付金额
        private void WriteBackContract(int contractID)
        {
            c_contract contract = dao.Load<c_contract>(contractID);
            if (contract != null)
            {
                object r = dao.GetFieldValue("select sum(FPayAmount) from c_contract_income where FContractID=" + contractID);
                contract.FPayedAmount = Convert.ToDecimal(r==null?0:r);
                dao.Update(contract);
                WriteBackIncomeInfo(contract);
            }
        }

        //http://localhost:8055/server/customer/ContractIncome.ashx?cmd=WriteAllContract
        [NoNeedLogin]
        [UseDB(Transaction = true)]
        public void WriteAllContract()
        {
            IList<c_contract> list = dao.GetList<c_contract>();
            foreach (c_contract item in list)
            {
               WriteBackIncomeInfo(item);
            }
            jsonResult = JSONHelper.SuccessJson;
        }


        private void WriteBackIncomeInfo(c_contract contract)
        {
            IList<c_contract_income> list = dao.GetList<c_contract_income>("FContractID=" + contract.FID);
            String r = "";
            int i = 0;
            foreach (c_contract_income item in list)
            {
                i++;
                if (!string.IsNullOrEmpty(r))
                {
                    r += "<br>";
                }
                r += "<font color=blue>" + i + "</font>：" + item.FIncomeType;
                r += ",付款金额(元)：" + item.FPayAmount;
                r += ",付款时间：" + item.FPayDate.ToString("yyyy-MM-dd");
                r += ",开票金额(元)：" + item.FInvoiceAmount;
                r += ",开票时间：" + item.FVoiceDate.ToString("yyyy-MM-dd");
                r += ",备注：" + item.FNote;
                r += ",经办人：" + item.FUserName;
            }
            contract.FIncomeInfo = r;
            dao.Update(contract);
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            int incomeID = Convert.ToInt32(Request["FID"]);
            c_contract_income income = dao.Load<c_contract_income>(incomeID);
            dao.Delete(income);
            WriteBackContract(income.FContractID);
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<c_contract_income> list = dao.GetList<c_contract_income>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_contract_income> list = dao.GetList<c_contract_income>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}