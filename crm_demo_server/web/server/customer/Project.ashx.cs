﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.customer
{
    /// <summary>
    /// Supplier 的摘要说明
    /// </summary>
    public class Project : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_project obj = ToPo<c_project>();
            //去掉时间部分
            obj.FDeadlineBeg = obj.FDeadlineBeg.Date;
            obj.FDeadlineEnd = obj.FDeadlineEnd.Date;
            if (obj.FID == 0)
            {
                obj.FIsBankroll = "否";
                obj.FIsHasContract = "否";
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            WriteBackCustomer(obj.FCustID);
            jsonResult = JSONHelper.SuccessJson;
        }
        //http://localhost:8055/server/customer/project.ashx?cmd=WriteAllCustomer&start=0&limit=1000
        [NoNeedLogin]
        [UseDB(Transaction = true)]
        public void WriteAllCustomer()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            IList<c_customer> list = dao.GetList<c_customer>("", "", start, limit);
            foreach (c_customer item in list)
            {
                WriteBackCustomer(item.FID);
            }
            jsonResult = JSONHelper.SuccessJson;
        }


        private void WriteBackCustomer(int custID)
        {
            c_customer customer = dao.Load<c_customer>(custID);
            if (customer == null)
            {
                return;
            }
            IList<c_project> list = dao.GetList<c_project>("FCustID=" + custID);
            String r = "";
            int i = 0;
            foreach (c_project item in list)
            {
                i++;
                if (!string.IsNullOrEmpty(r))
                {
                    r += "<br>";
                }
                r += "<font color=blue>项目" + i + "</font>：" + item.FName;
                r += ",项目建设内容：" + item.FContent;
                r += ",建设期限：" + item.FDeadline;
                r += ",总投资额：" + item.FAmount;
                r += ",项目地址：" + item.FAddress;
                r += ",是否获取资金：" + item.FIsBankroll;
                r += ",是否签合同：" + item.FIsHasContract;
                r += ",项目来源：" + item.FSource;
                r += ",项目建设阶段：" + item.FStatus;
            }
            customer.FProjectInfo = r;
            dao.Update(customer);
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            int projectID = Convert.ToInt32(Request["FID"]);
            c_project project = dao.Load<c_project>(projectID);
            dao.Delete(project);
            WriteBackCustomer(project.FCustID);
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<c_project> list = dao.GetList<c_project>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            int custID = -1;
            if (!String.IsNullOrEmpty(Request["FCustID"]))
            {
                custID = Convert.ToInt32(Request["FCustID"]);
            }
            IList<c_project> list = dao.GetList<c_project>("FCustID=" + custID);
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}