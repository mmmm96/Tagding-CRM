﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.customer
{
    /// <summary>
    /// Supplier 的摘要说明
    /// </summary>
    public class ProjectTrace : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_project_trace obj = ToPo<c_project_trace>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            WriteBackCustomer(obj.FCustID);
            jsonResult = JSONHelper.SuccessJson;
        }


        //http://localhost:8055/server/customer/ProjectTrace.ashx?cmd=WriteAllCustomer&start=0&limit=1000
        [NoNeedLogin]
        [UseDB(Transaction = true)]
        public void WriteAllCustomer()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            IList<c_customer> list = dao.GetList<c_customer>("", "", start, limit);
            foreach (c_customer item in list)
            {
                WriteBackCustomer(item.FID);
            }
            jsonResult = JSONHelper.SuccessJson;
        }


        private void WriteBackCustomer(int custID)
        {
            c_customer customer = dao.Load<c_customer>(custID);
            if (customer == null)
            {
                return;
            }
            IList<c_project_trace> list = dao.GetList<c_project_trace>("FCustID=" + custID, "FCreateDate desc,FID desc");
            String r = "";
            int i = 0;
            foreach (c_project_trace item in list)
            {
                i++;
                if (!string.IsNullOrEmpty(r))
                {
                    r += "<br>";
                }
                r += "<font color=blue>跟进" + i + "</font>：" + item.FCreateDate.ToString("yyyy-MM-dd");
                r += ",跟进内容：" + item.FContent;
                r += ",客方联系人：" + item.FLinkMan;
                r += ",联系电话：" + item.FLinkPhone;
                r += ",业务员：" + item.UserName;
            }
            customer.FTraceInfo = r;
            dao.Update(customer);
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            int id = Convert.ToInt32(Request["FID"]);
            c_project_trace trace = dao.Load<c_project_trace>(id);
            dao.Delete(trace);
            WriteBackCustomer(trace.FCustID);
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<c_project_trace> list = dao.GetList<c_project_trace>(out rowCount, sWhere, "FCreateDate desc,FID desc", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 待跟进信息
        /// </summary>
        [UseDB]
        public void GetMemoList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            //当天或后期的
            sWhere = sWhere.and("FToDate>=" + DateTime.Now.ToString("yyyy-MM-dd").wrap());
            //加权限控制
            if (!GMLogin.isAdminUser())
            {
                sWhere = sWhere.and("FUserID=" + GMLogin.UserID);
            }
            int rowCount;
            IList<c_project_trace> list = dao.GetList<c_project_trace>(out rowCount, sWhere, "FToDate asc,FCreateDate asc,FID desc", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 待跟进信息
        /// </summary>
        [UseDB]
        public void GetMemoListAll()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            //加权限控制
            if (!GMLogin.isAdminUser())
            {
                sWhere = sWhere.and("FUserID=" + GMLogin.UserID);
            }
            int rowCount;
            IList<c_project_trace> list = dao.GetList<c_project_trace>(out rowCount, sWhere, "FCreateDate desc,FID desc", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }
    }
}