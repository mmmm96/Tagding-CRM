﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.customer
{
    /// <summary>
    /// Supplier 的摘要说明
    /// </summary>
    public class ContractExpend : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_contract_expend obj = ToPo<c_contract_expend>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            WriteBackExpendInfo(obj.FContractID);
            jsonResult = JSONHelper.SuccessJson;
        }


        //http://localhost:8055/server/customer/ContractExpend.ashx?cmd=WriteAllContract
        [NoNeedLogin]
        [UseDB(Transaction = true)]
        public void WriteAllContract()
        {
            IList<c_contract> list = dao.GetList<c_contract>();
            foreach (c_contract item in list)
            {
                WriteBackExpendInfo(item.FID);
            }
            jsonResult = JSONHelper.SuccessJson;
        }


        private void WriteBackExpendInfo(int contractID)
        {
            c_contract contract = dao.Load<c_contract>(contractID);
            if (contract==null)
            {
                return;
            }
            IList<c_contract_expend> list = dao.GetList<c_contract_expend>("FContractID=" + contractID);
            String r = "";
            int i = 0;
            foreach (c_contract_expend item in list)
            {
                i++;
                if (!string.IsNullOrEmpty(r))
                {
                    r += "<br>";
                }
                r += "<font color=blue>" + i + "</font>：" + item.FExpendType;
                r += ",金额(元)：" + item.FAmount;
                r += ",付款时间：" + item.FCreateDate.ToString("yyyy-MM-dd");
                r += ",备注：" + item.FNote;
                r += ",经办人：" + item.FUserName;
            }
            contract.FExpendInfo = r;
            dao.Update(contract);
        }


        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            int id = Convert.ToInt32(Request["FID"]);
            c_contract_expend expend = dao.Load<c_contract_expend>(id);
            dao.Delete(expend);
            WriteBackExpendInfo(expend.FContractID);
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<c_contract_expend> list = dao.GetList<c_contract_expend>(out rowCount, sWhere, "FCreateDate", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }
    }
}