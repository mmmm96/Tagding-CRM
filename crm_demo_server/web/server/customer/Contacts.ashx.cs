﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.customer
{
    /// <summary>
    /// Supplier 的摘要说明
    /// </summary>
    public class Contacts : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_contacts obj = ToPo<c_contacts>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            WriteBackCustomer(obj.FCustID);
            jsonResult = JSONHelper.SuccessJson;
        }

        private void WriteBackCustomer(int custID)
        {
            c_customer customer = dao.Load<c_customer>(custID);
            if (customer==null)
            {
                return;
            }
            IList<c_contacts> list = dao.GetList<c_contacts>("FCustID=" + custID);
            String r = "";
            int i = 0;
            foreach(c_contacts item in list){
                i++;
                if (!string.IsNullOrEmpty(r))
                {
                    r += "<br>";
                }
                r += "<font color=blue>联系人" + i + "</font>：" + item.FName;
                r += ",职位：" + item.FPosition;
                r += ",传真：" + item.FFax;
                r += ",手机：" + item.FPhone;
                r += ",联系人：" + item.FName;
                r += ",固定电话：" + item.FTelphone;
                r += ",QQ：" + item.FQQ;
                r += ",备注：" + item.FNote;
                r += ",性别：" + item.FSex;
                r += ",籍贯：" + item.FOriginPlace;
                r += ",部门：" + item.FDept;
                r += ",分管事务：" + item.FForProject;
            }
            customer.FContactsInfo = r;
            dao.Update(customer);
        }

        //http://localhost:8055/server/customer/Contacts.ashx?cmd=WriteAllCustomer&start=0&limit=1000
        [NoNeedLogin]
        [UseDB(Transaction = true)]
        public void WriteAllCustomer()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            IList<c_customer> list = dao.GetList<c_customer>("", "", start, limit);
            foreach (c_customer item in list)
            {
                WriteBackCustomer(item.FID);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            int contactsID = Convert.ToInt32(Request["FID"]);
            c_contacts contact = dao.Load<c_contacts>(contactsID);
            dao.Delete(contact);
            WriteBackCustomer(contact.FCustID);
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<c_contacts> list = dao.GetList<c_contacts>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_contacts> list = dao.GetList<c_contacts>("FCustID="+Request["custID"]);
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}