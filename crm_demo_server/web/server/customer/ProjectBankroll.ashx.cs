﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.customer
{
    /// <summary>
    /// Supplier 的摘要说明
    /// </summary>
    public class ProjectBankroll : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_project_bankroll obj = ToPo<c_project_bankroll>();
            //去掉时间部分
            obj.FDeadlineBeg = obj.FDeadlineBeg.Date;
            obj.FDeadlineEnd = obj.FDeadlineEnd.Date;
            if (obj.FID == 0)
            {
                dao.Save(obj);
                WriteBackProject(obj.FProjectID);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 更新项目是否获得资金
        /// </summary>
        /// <param name="projectID"></param>
        private void WriteBackProject(int projectID)
        {
            c_project project = dao.Get<c_project>(projectID);
            if (project == null)
            {
                return;
            }
            object sum = dao.GetFieldValue("select sum(*) from c_project_bankroll where FProjectID=" + projectID);
            if(sum!=null && Convert.ToInt32(sum)>0){
                project.FIsBankroll = "是";
                dao.Save(project);
            }
            else
            {
                project.FIsBankroll = "否";
                dao.Save(project);
            }
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            //更新项目是否获得资金
            c_project_bankroll bankroll = dao.Get<c_project_bankroll>(Convert.ToInt32(Request["FID"]));
            if (bankroll != null)
            {
                dao.Delete(bankroll);
                WriteBackProject(bankroll.FProjectID);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<c_project_bankroll> list = dao.GetList<c_project_bankroll>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }
    }
}