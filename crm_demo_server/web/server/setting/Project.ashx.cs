﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.setting
{
    public class Project : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_wz_project obj = ToPo<c_wz_project>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.Delete(new c_wz_project() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

          /// <summary>
          /// 删除(多选)
          /// </summary>
         [UseDB(Transaction = true)]
          public void DeleteAll()
          {
             dao.ExcuteByHSql("delete from c_wz_project where FID in (" + Request["FIDS"].ToString() + ")");
             jsonResult = JSONHelper.SuccessJson;
          }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            int rowCount;
            IList<c_wz_project> list = dao.GetList<c_wz_project>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_wz_project> list = dao.GetList<c_wz_project>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}