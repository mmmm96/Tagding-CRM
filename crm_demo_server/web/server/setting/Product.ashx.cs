﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.setting
{
    public class Product : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_product obj = ToPo<c_product>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.Delete(new c_product() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除(多选)
        /// </summary>
        [UseDB(Transaction = true)]
        public void DeleteAll()
        {
            dao.ExcuteByHSql("delete from c_product where FID in (" + Request["FIDS"].ToString() + ")");
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            int rowCount;
            IList<c_product> list = dao.GetList<c_product>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            string where = Request["where"];
            IList<c_product> list = dao.GetList<c_product>();
            var options = JSONHelper.CustomList((IList)list, "*", (one,value) =>
            {
                c_product product = (c_product)value;
                if(where.IndexOf(","+product.FID+",")>=0)
                {
                    one.Add("disabled",true);
                }
            });
            jsonResult = JSONHelper.ToSucJson("options", options);
        }
    }
}