﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.setting
{
    public class ProductType : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_product_type obj = ToPo<c_product_type>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            //先删除分类下的产品
            dao.ExcuteByHSql(@"delete from c_product where FTypeID =" + Convert.ToInt32(Request["FID"]));
            //删除分类
            dao.Delete(new c_product_type() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            int rowCount;
            IList<c_product_type> list = dao.GetList<c_product_type>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_product_type> list = dao.GetList<c_product_type>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }

        [UseDB]
        /// <summary>
        /// 获取产品类别树列表
        /// </summary>
        public void GetProductTypeTree()
        {
            IList<c_product_type> list = GetProductTypeAll();
            IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
            MakeTree(list, 0, jsonList);
            jsonResult = JSONHelper.ToSucJson("data", jsonList);
        }

        private IList<c_product_type> GetProductTypeAll()
        {
            return dao.GetList<c_product_type>("", "FParentID,FSort");
        }

        private int MakeTree(IList<c_product_type> listTree, int parent, IList<Dictionary<string, object>> outputTree)
        {
            int childCount = 0;
            foreach (c_product_type item in listTree)
            {
                if (item.FParentID == parent)
                {
                    childCount++;
                    Dictionary<string, object> josonDict = new Dictionary<string, object>();
                    josonDict.Add("FID", item.FID);
                    josonDict.Add("FName", item.FName);
                    josonDict.Add("FParentID", item.FParentID);
                    josonDict.Add("FSort", item.FSort);
                    josonDict.Add("FNote", item.FNote);
                    IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
                    int myChildCount = MakeTree(listTree, item.FID, jsonList);
                    if (myChildCount > 0)
                    {
                        josonDict.Add("children", jsonList);
                    }
                    outputTree.Add(josonDict);
                }
            }
            return childCount;
        }
    }
}