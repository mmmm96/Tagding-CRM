﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.business
{
    public class Stocking : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_stocking obj = ToPo<c_stocking>();
            if (obj.FID == 0)
            {
                int stockingID = dao.Save(obj);
                IList<c_stocking_detail> listdata = JSONHelper.ToObject<IList<c_stocking_detail>>(Request["listdata"]);
                foreach (c_stocking_detail item in listdata)
                {
                    item.FStockingID = stockingID;
                    dao.Save(item);
                }
            }
            else
            {
                dao.ExcuteByHSql("delete from c_stocking_detail where FStockingID =" + obj.FID);
                dao.Update(obj);
                IList<c_stocking_detail> listdata = JSONHelper.ToObject<IList<c_stocking_detail>>(Request["listdata"]);
                foreach (c_stocking_detail item in listdata)
                {
                    item.FStockingID = obj.FID;
                    dao.Save(item);
                }
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.ExcuteByHSql("delete from c_stocking_detail where FStockingID =" + Convert.ToInt32(Request["FID"]));
            dao.Delete(new c_stocking() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

          /// <summary>
          /// 删除(多选)
          /// </summary>
         [UseDB(Transaction = true)]
          public void DeleteAll()
          {
             dao.ExcuteByHSql("delete from c_stocking where FID in (" + Request["FIDS"].ToString() + ")");
             jsonResult = JSONHelper.SuccessJson;
          }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            if (!GMLogin.isAdminUser())
            {
                sWhere = sWhere.and("FUserID=" + GMLogin.UserID);
            }
            int rowCount;
            IList<c_stocking> list = dao.GetList<c_stocking>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_stocking> list = dao.GetList<c_stocking>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}