﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.business
{
    public class ApplyDetail : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_apply_detail obj = ToPo<c_apply_detail>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.Delete(new c_apply_detail() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

          /// <summary>
          /// 删除(多选)
          /// </summary>
         [UseDB(Transaction = true)]
          public void DeleteAll()
          {
             dao.ExcuteByHSql("delete from c_apply_detail where FID in (" + Request["FIDS"].ToString() + ")");
             jsonResult = JSONHelper.SuccessJson;
          }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            string sWhere = Request["where"];
            IList<c_apply_detail> list = dao.GetList<c_apply_detail>(sWhere);
            var options = JSONHelper.CustomList((IList)list, "*", (one, value) =>
            {
                c_apply_detail detail = (c_apply_detail)value;
                one.Add("Total", detail.FPrice*detail.FQuantity);
            });
            jsonResult = JSONHelper.ToSucJson("listdata", options);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_apply_detail> list = dao.GetList<c_apply_detail>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}