﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.business
{
    public class Apply : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_apply obj = ToPo<c_apply>();
            if (obj.FID == 0)
            {
                int applyID = dao.Save(obj);
                IList<c_apply_detail> listdata = JSONHelper.ToObject<IList<c_apply_detail>>(Request["listdata"]);
                foreach (c_apply_detail item in listdata)
                {
                    item.FApplyID = applyID;
                    dao.Save(item);
                }
            }
            else
            {
                dao.ExcuteByHSql("delete from c_apply_detail where FApplyID =" + obj.FID);
                dao.Update(obj);
                IList<c_apply_detail> listdata = JSONHelper.ToObject<IList<c_apply_detail>>(Request["listdata"]);
                foreach (c_apply_detail item in listdata)
                {
                    item.FApplyID = obj.FID;
                    dao.Save(item);
                }
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.ExcuteByHSql("delete from c_apply_detail where FApplyID =" + Convert.ToInt32(Request["FID"]));
            dao.Delete(new c_apply() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

          /// <summary>
          /// 删除(多选)
          /// </summary>
         [UseDB(Transaction = true)]
          public void DeleteAll()
          {
             dao.ExcuteByHSql("delete from c_apply where FID in (" + Request["FIDS"].ToString() + ")");
             jsonResult = JSONHelper.SuccessJson;
          }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            if (!GMLogin.isAdminUser())
            {
                sWhere = sWhere.and("(FApplyUser=" + GMLogin.UserID + "or FToApprover=" + GMLogin.UserID+")");
            }
            int rowCount;
            IList<c_apply> list = dao.GetList<c_apply>(out rowCount, sWhere, "FID DESC", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }


        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_apply> list = dao.GetList<c_apply>(" FApproveStatus=3 and FApplyNo not in (select FApplyNo from c_delivery where FIsConfirm=1 )");
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }

        /// <summary>
        /// 获取一条数据根据FID
        /// </summary>
        [UseDB]
        public void GetOne()
        {
            c_apply one = dao.Get<c_apply>(int.Parse(Request["FID"]));
            jsonResult = JSONHelper.ToSucJson("data", one);
        }

        /// <summary>
        /// 打印采购单
        /// </summary>
        [UseDB]
        public void PrintApply()
        {
            c_apply one = dao.Get<c_apply>(int.Parse(Request["FID"]));
            IList<c_apply_detail> detaillist = null;
            if (one != null)
            {
                detaillist = dao.GetList<c_apply_detail>(String.Format("FApplyID={0}", one.FID));
            }
            jsonResult = JSONHelper.AddF("applyData", one)
                                   .AddF("applyDetail", detaillist).ToSucJson();
        }
    }
}