﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.business
{
    public class Delivery : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_delivery obj = ToPo<c_delivery>();
            if (obj.FID == 0)
            {
                int DeliveryID = dao.Save(obj);
                IList<c_delivery_detail> listdata = JSONHelper.ToObject<IList<c_delivery_detail>>(Request["listdata"]);
                foreach (c_delivery_detail item in listdata)
                {
                    item.FDeliveryID = DeliveryID;
                    dao.Save(item);
                }
            }
            else
            {
                dao.ExcuteByHSql("delete from c_delivery_detail where FDeliveryID =" + obj.FID);
                dao.Update(obj);
                IList<c_delivery_detail> listdata = JSONHelper.ToObject<IList<c_delivery_detail>>(Request["listdata"]);
                foreach (c_delivery_detail item in listdata)
                {
                    item.FDeliveryID = obj.FID;
                    dao.Save(item);
                }
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.ExcuteByHSql("delete from c_delivery_detail where FDeliveryID =" + Convert.ToInt32(Request["FID"]));
            dao.Delete(new c_delivery() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

          /// <summary>
          /// 删除(多选)
          /// </summary>
         [UseDB(Transaction = true)]
          public void DeleteAll()
          {
             dao.ExcuteByHSql("delete from c_delivery where FID in (" + Request["FIDS"].ToString() + ")");
             jsonResult = JSONHelper.SuccessJson;
          }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            if (!GMLogin.isAdminUser())
            {
                sWhere = sWhere.and("(FDeliveryUser=" + GMLogin.UserID + "or FToApprover=" + GMLogin.UserID + ")");
            }
            int rowCount;
            IList<c_delivery> list = dao.GetList<c_delivery>(out rowCount, sWhere, "FID DESC", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取确认列表
        /// </summary>
        [UseDB]
        public void GetConfirmList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            if (!GMLogin.isAdminUser())
            {
                sWhere = sWhere.and("(FShippingUser=" + GMLogin.UserID + " or FDeliveryUser=" + GMLogin.UserID+")");
            }
            int rowCount;
            IList<c_delivery> list = dao.GetList<c_delivery>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_delivery> list = dao.GetList<c_delivery>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }


        /// <summary>
        /// 确认收货,如果错误则发送信息
        /// </summary>
        [UseDB(Transaction = true)]
        public void Confirm()
        {
            string msg = "";
            c_delivery obj = ToPo<c_delivery>();    
            dao.Update(obj);
            IList<c_delivery_detail> listdata = JSONHelper.ToObject<IList<c_delivery_detail>>(Request["listdata"]);
            foreach (c_delivery_detail item in listdata)
            {
                //如果发货数量与实际收到数量不符，则提示
                if (item.FFactQuantity!=item.FQuantity)
                {
                    msg = msg + "货物:" + item.ProductName + ",应发货:" + item.FQuantity + item.ProductUnitName + ",实际收到:" + item.FFactQuantity + item.ProductUnitName + ";";
                }
                dao.Update(item);
            }

            if (msg!="")
            {
                //发送短信提醒
                
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取一条数据根据FID
        /// </summary>
        [UseDB]
        public void GetOne()
        {     
            c_delivery one = dao.Get<c_delivery>(int.Parse(Request["FID"]));
            jsonResult = JSONHelper.ToSucJson("data", one);
        }

         /// <summary>
        /// 打印采购单
        /// </summary>
        [UseDB]
        public void PrintDelivery()
        {
            c_delivery one = dao.Get<c_delivery>(int.Parse(Request["FID"]));
            IList<c_delivery_detail> detaillist = null;
            if (one != null)
            {
                detaillist = dao.GetList<c_delivery_detail>(String.Format("FDeliveryID={0}", one.FID));
            }
            jsonResult = JSONHelper.AddF("deliveryData", one)
                                   .AddF("deliveryDetail", detaillist).ToSucJson();
        }
    }
}