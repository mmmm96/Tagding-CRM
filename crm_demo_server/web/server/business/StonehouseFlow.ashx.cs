﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.business
{
    public class StonehouseFlow : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        { 
            c_stonehouseflow obj = ToPo<c_stonehouseflow>();
            if (obj.FID == 0)
            {
                int stonehouseflowID = dao.Save(obj);
                IList<c_stonehouseflow_detail> listdata = JSONHelper.ToObject<IList<c_stonehouseflow_detail>>(Request["listdata"]);
                foreach(c_stonehouseflow_detail item in listdata)
                {
                    item.FStonehouseflowID = stonehouseflowID;
                    dao.Save(item);
                }
            }
            else
            {
                dao.ExcuteByHSql("delete from c_stonehouseflow_detail where FStonehouseflowID =" + obj.FID);
                dao.Update(obj);
                IList<c_stonehouseflow_detail> listdata = JSONHelper.ToObject<IList<c_stonehouseflow_detail>>(Request["listdata"]);
                foreach (c_stonehouseflow_detail item in listdata)
                {
                    item.FStonehouseflowID = obj.FID;
                    dao.Save(item);
                }
            }
           

            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.ExcuteByHSql("delete from c_stonehouseflow_detail where FStonehouseflowID =" + Convert.ToInt32(Request["FID"]));
            dao.Delete(new c_stonehouseflow() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

          /// <summary>
          /// 删除(多选)
          /// </summary>
         [UseDB(Transaction = true)]
          public void DeleteAll()
          {
             dao.ExcuteByHSql("delete from c_stonehouseflow where FID in (" + Request["FIDS"].ToString() + ")");
             jsonResult = JSONHelper.SuccessJson;
          }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            if(!GMLogin.isAdminUser())
            {
                sWhere = sWhere.and("FUserID=" + GMLogin.UserID);
            }
            int rowCount;
            IList<c_stonehouseflow> list = dao.GetList<c_stonehouseflow>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {   
            IList<c_stonehouseflow> list = dao.GetList<c_stonehouseflow>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }


        /// <summary>
        /// 确认出入库
        /// </summary>
        [UseDB(Transaction = true)]
        public void Sure()
        {
            c_stonehouseflow obj = ToPo<c_stonehouseflow>();
            if (obj.FType == 1) //入库
            {
                //遍历明细表
                IList<c_stonehouseflow_detail> list = dao.GetList<c_stonehouseflow_detail>("FStonehouseFlowID="+obj.FID);
                foreach (c_stonehouseflow_detail item in list)
                {
                    c_stock stock = dao.Get<c_stock>("FProductID=" + item.FProductID + "and FStonehouseID=" + obj.FStonehouseID);
                    if (stock == null)
                    {
                        //原来不存在该条产品和仓库的数据,插入新数据
                        stock = new c_stock();
                        stock.FID = 0;
                        stock.FProductID = item.FProductID;
                        stock.FStonehouseID = obj.FStonehouseID;
                        stock.FQuantity = item.FQuantity;
                        dao.Save(stock);   
                    }
                    else
                    {
                        //原来存在该条产品和仓库的数据,更新数据
                        stock.FQuantity = stock.FQuantity + item.FQuantity;
                        dao.Update(stock);
                    }
                }
                //更新主表状态
                obj.FStatus = 1;
                dao.Update(obj);
                jsonResult = JSONHelper.SuccessJson;
            }
            else  //出库
            {
                Boolean sign = true;
                 //遍历明细表
                IList<c_stonehouseflow_detail> list = dao.GetList<c_stonehouseflow_detail>("FStonehouseFlowID="+obj.FID);
                foreach (c_stonehouseflow_detail item in list)
                {
                    c_stock stock = dao.Get<c_stock>("FProductID=" + item.FProductID + "and FStonehouseID=" + obj.FStonehouseID);
                    if (stock == null)
                    {
                        sign = false;
                        //没有库存，不允许提交
                        jsonResult = JSONHelper.ToErroJson(item.ProductName+"库存不足,无法出库", "203");
                    }
                    else
                    {
                        if (stock.FQuantity < item.FQuantity)
                        {
                            sign = false;
                            jsonResult = JSONHelper.ToErroJson(item.ProductName + "库存不足,无法出库", "203");
                        }
                    }
                }
                if (sign)
                {
                    foreach (c_stonehouseflow_detail item in list)
                    {
                        c_stock stock = dao.Get<c_stock>("FProductID=" + item.FProductID + "and FStonehouseID=" + obj.FStonehouseID);
                        //原来存在该条产品和仓库的数据,更新数据
                        stock.FQuantity = stock.FQuantity - item.FQuantity;
                        dao.Update(stock);
                        //更新入库流水单状态
                        obj.FStatus = 1;

                    }
                    dao.Update(obj);
                    jsonResult = JSONHelper.SuccessJson;
                }
                }
            }
        
    }
}