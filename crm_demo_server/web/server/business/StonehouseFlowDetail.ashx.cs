﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.business
{
    public class StonehouseFlowDetail : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_stonehouseflow_detail obj = ToPo<c_stonehouseflow_detail>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.Delete(new c_stonehouseflow_detail() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

          /// <summary>
          /// 删除(多选)
          /// </summary>
         [UseDB(Transaction = true)]
          public void DeleteAll()
          {
             dao.ExcuteByHSql("delete from c_stonehouseflow_detail where FID in (" + Request["FIDS"].ToString() + ")");
             jsonResult = JSONHelper.SuccessJson;
          }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            string sWhere = Request["where"];
            IList<c_stonehouseflow_detail> list = dao.GetList<c_stonehouseflow_detail>(sWhere);
            jsonResult = JSONHelper.ToSucJson("listdata",(IList)list);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_stonehouseflow_detail> list = dao.GetList<c_stonehouseflow_detail>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}