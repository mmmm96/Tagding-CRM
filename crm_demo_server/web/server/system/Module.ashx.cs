﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.system
{
    /// <summary>
    /// Module 的摘要说明
    /// </summary>
    public class Module : BaseHandler
    {
        /// <summary>
        /// 保存角色
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            t_module obj = ToPo<t_module>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            //先删除模块对应的菜单
            dao.ExcuteByHSql("delete from t_menu where FModuleID=" + Request["FID"]);
            dao.Delete(new t_module() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取模块列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<t_module> list = dao.GetList<t_module>(out rowCount, sWhere, "FSort", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }
    }
}