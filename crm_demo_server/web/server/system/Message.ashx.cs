﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;

namespace ReportFlat.Web.server.system
{
    public partial class Message : BaseHandler
    {
        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            sWhere = sWhere.and("FReceiverID=" + GMLogin.UserID);
            int rowCount;
            IList<t_message> list = dao.GetList<t_message>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }


        /// <summary>
        /// 修改为已读
        /// </summary>
        [UseDB(Transaction = true)]
        public void Read()
        {
            t_message message = dao.Get<t_message>(int.Parse(Request["FID"]));
            message.FStatus = 1;
            dao.Update(message);
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取未读的MESSAGE数量
        /// </summary>
        [UseDB]
        public void GetCount()
        {
            string sql = "from t_message where FStatus<>1 and FReceiverID="+GMLogin.UserID;
            int count = dao.GetRowCount(sql);
            jsonResult = JSONHelper.ToSucJson("count", count);
        }
    }
}
