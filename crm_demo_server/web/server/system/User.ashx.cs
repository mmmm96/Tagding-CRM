﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;

namespace ReportFlat.Web.server.system
{
    /// <summary>
    /// User 的摘要说明
    /// </summary>
    public class User : BaseHandler
    {
        /// <summary>
        /// 保存用户
        /// </summary>
        [UseDB(Transaction = true)]
        public void SaveUser()
        {
            t_user obj = ToPo<t_user>("FPassWord");
            if (!String.IsNullOrEmpty(Request["FPassWord"]))
            {
                obj.FPassWord = Request["FPassWord"];
            }
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 保存用户
        /// </summary>
        [UseDB(Transaction = true)]
        public void MoveUser()
        {
            int orgUserId = Convert.ToInt32(Request["FID"]);
            int newUserID = Convert.ToInt32(Request["FUserID"]);
            dao.ExcuteByHSql(String.Format("update c_contract set FOrgUserID={0},FUserID={1} where FUserID={2}", orgUserId, newUserID, orgUserId));
            dao.ExcuteByHSql(String.Format("update c_customer set FOrgUserID={0},FUserID={1} where FUserID={2}", orgUserId, newUserID, orgUserId));
            dao.ExcuteByHSql(String.Format("update c_project_trace set FOrgUserID={0},FUserID={1} where FUserID={2}", orgUserId, newUserID, orgUserId));
            t_user user = dao.Get<t_user>(orgUserId);
            if (user != null)
            {
                user.FNote = Request["FNote"];
                dao.Update(user);
            }
            jsonResult = JSONHelper.SuccessJson;
        }
        

        /// <summary>
        /// 修改密码
        /// </summary>
        [UseDB(Transaction = true)]
        public void ModifyPsw()
        {
            t_user user = this.GMLogin.User;
            if (user != null)
            {
                user.FPassWord = Request["FPassWord"];
                dao.Update(user);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        [UseDB(Transaction = true)]
        public void DeleteUser()
        {
            dao.Delete(new t_user() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        [UseDB]
        public void GetUserList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<t_user> list = dao.GetList<t_user>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        [UseDB]
        public void GetNameList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<v_user_name> list = dao.GetList<v_user_name>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        [UseDB]
        public void GetUserOptions()
        {
            IList<t_user> list = dao.GetList<t_user>("","FID");
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }

        [UseDB]
        public void GetRealNames()
        {
            string sqlUser = "SELECT DISTINCT FRealName FROM t_user WHERE FID>0 AND FRealName IS NOT NULL AND FRealName<>'' ORDER BY FRealName";
            IList listUser = dao.QueryBySql(sqlUser);
            IList<Dictionary<string, object>> result = new List<Dictionary<string, object>>();
            foreach(Object realName in listUser){
                if (realName != null && realName.ToString().IndexOf(",") >= 0)
                {
                    continue;
                }
                Dictionary<string, object> item = new Dictionary<string, object>();
                item.Add("FID", realName);
                item.Add("FName", realName);
                result.Add(item);
            }
            jsonResult = JSONHelper.ToSucJson("options", (IList)result);
        }
    }
}