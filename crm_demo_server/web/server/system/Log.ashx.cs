﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ReportFlat.bussiness;
using ReportFlat.DataAccess;
using ReportFlat.Common;

namespace ReportFlat.Web.server.system
{
    public partial class Log : BaseHandler
    {
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            BusLog busSystem = new BusLog();
            int rowCount;
            IList list = busSystem.GetList(sWhere, start, limit, out rowCount);
            jsonResult = JSONHelper.ToSucJson(list, rowCount);
        }
    }
}
