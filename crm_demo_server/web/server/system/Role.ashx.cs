﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;

namespace ReportFlat.Web.server.system
{
    /// <summary>
    /// Role 的摘要说明
    /// </summary>
    public class Role : BaseHandler
    {
        /// <summary>
        /// 保存角色
        /// </summary>
        [UseDB(Transaction = true)]
        public void SaveRole()
        {
            t_role obj = ToPo<t_role>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        [UseDB(Transaction = true)]
        public void DeleteRole()
        {
            dao.ExcuteByHSql("delete from t_roleuser where FRoleID=" + Request["FID"]);
            dao.Delete(new t_role() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取角色列表
        /// </summary>
        [UseDB]
        public void GetRoleList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<t_role> list = dao.GetList<t_role>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 保存角色用户
        /// </summary>
        [UseDB(Transaction = true)]
        public void SaveRoleUser()
        {
            t_roleuser obj = ToPo<t_roleuser>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除角色用户
        /// </summary>
        [UseDB(Transaction = true)]
        public void DeleteRoleUser()
        {
            dao.Delete(new t_roleuser() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取角色用户列表
        /// </summary>
        [UseDB]
        public void GetRoleUserList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<t_roleuser> list = dao.GetList<t_roleuser>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }
    }
}