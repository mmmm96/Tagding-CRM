﻿using ReportFlat.Common;
using ReportFlat.DataAccess;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReportFlat.Web.server.system
{
    public partial class Excel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string sWhere = Server.UrlDecode(Request.Params["where"].ToString());
            int FWxID = Convert.ToInt32(Request["FWxID"]);
            DAOAssist dao = new DAOAssist();
            String[] Columns = { "客户名称", "客户类别", "所属行业", "省份", "城市","城市",
                                 "地址", "是否万家企业", "是否高耗能企业", "法人", "联系人信息", "项目信息","备注"};
            string sql = "from c_customer where  " + sWhere + " order by FID";
            IList list = dao.QueryByHSql(sql);
            String fileName = "兑换记录" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
            CreateExcel(Columns, list, fileName);
            dao.CloseSession();
        }

        private string hidePhone(object phone)
        {
            if (phone != null && phone.ToString().Trim().Length == 11)
            {
                string result = phone.ToString().Trim();
                return result.Substring(0, 3) + "****" + result.Substring(7, 4);
            }
            else
            {
                return phone==null?phone.ToString():"";
            }
        }
        
        public void CreateExcel(String[] Columns,IList list, string FileName)
        {
            HttpResponse resp;
            resp = Page.Response;
            resp.ContentEncoding = System.Text.Encoding.GetEncoding("GB2312");
            resp.AppendHeader("Content-Disposition", "attachment;filename=" + FileName);
            string colHeaders = "", ls_item = "";

            int i = 0;
            int cl = Columns.Length;

            //取得数据表各列标题，各标题之间以t分割，最后一个列标题后加回车符 
            for (i = 0; i < cl; i++)
            {
                if (i == (cl - 1))//最后一列，加n
                {
                    colHeaders += Columns[i] + "\r\n";
                }
                else
                {
                    colHeaders += Columns[i] + ",";
                }

            }
            resp.Write(colHeaders);
            //向HTTP输出流中写入取得的数据信息 

            //逐行处理数据   
            foreach (Object[] row in list)
            {
                //当前行数据写入HTTP输出流，并且置空ls_item以便下行数据     
                for (i = 0; i < cl; i++)
                {
                    if (i == (cl - 1))//最后一列，加n
                    {
                        ls_item += GetString(row[i]) + "\r\n";
                    }
                    else
                    {
                        ls_item += GetString(row[i]) + ",";
                    }
                }
                resp.Write(ls_item);
                ls_item = "";

            }
            resp.End();
        }

        private string GetString(object s){
            if(s==null){
                return "";
            }
            return s.ToString();
        }

    }
}