﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.system
{
    public class Dept : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            t_dept obj = ToPo<t_dept>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.Delete(new t_dept() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request["start"]);
            int limit = int.Parse(Request["limit"]);
            string sWhere = Request["where"];
            int rowCount;
            IList<t_dept> list = dao.GetList<t_dept>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<t_dept> list = dao.GetList<t_dept>();
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }

        [UseDB]
        /// <summary>
        /// 获取部门树列表
        /// </summary>
        public void GetDeptTree()
        {
            IList<t_dept> list = GetDeptAll();
            IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
            MakeTree(list, 0, jsonList);
            jsonResult = JSONHelper.ToSucJson("data", jsonList);
        }

        private IList<t_dept> GetDeptAll()
        {
            return dao.GetList<t_dept>("", "FParentID,FSort");
        }

        private int MakeTree(IList<t_dept> listTree, int parent, IList<Dictionary<string, object>> outputTree)
        {
            int childCount = 0;
            foreach (t_dept item in listTree)
            {
                if (item.FParentID == parent)
                {
                    childCount++;
                    Dictionary<string, object> josonDict = new Dictionary<string, object>();
                    josonDict.Add("FID", item.FID);
                    josonDict.Add("FName", item.FName);
                    josonDict.Add("FParentID", item.FParentID);
                    josonDict.Add("FSort", item.FSort);
                    josonDict.Add("FNote", item.FNote);
                    josonDict.Add("FDelFlag", item.FDelFlag);
                    josonDict.Add("FLeader", item.FLeader);
                    josonDict.Add("FUpLeader", item.FUpLeader);
                    IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
                    int myChildCount = MakeTree(listTree, item.FID, jsonList);
                    if (myChildCount > 0)
                    {
                        josonDict.Add("children", jsonList);
                    }
                    outputTree.Add(josonDict);
                }
            }
            return childCount;
        }
    }
}