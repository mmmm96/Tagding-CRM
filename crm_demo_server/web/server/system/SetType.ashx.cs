﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;


namespace ReportFlat.Web.server.system
{
    /// <summary>
    /// Supplier 的摘要说明
    /// </summary>
    public class SetType : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            c_set_type obj = ToPo<c_set_type>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 删除
        /// </summary>
        [UseDB(Transaction = true)]
        public void Delete()
        {
            dao.Delete(new c_set_type() { FID = Convert.ToInt32(Request["FID"]) });
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<c_set_type> list = dao.GetList<c_set_type>(out rowCount, sWhere, "FNote", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        /// <summary>
        /// 获取下拉列表
        /// </summary>
        [UseDB]
        public void GetOptions()
        {
            IList<c_set_type> list = dao.GetList<c_set_type>("FType=" + Request["FType"]);
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}