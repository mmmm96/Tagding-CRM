﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;

namespace ReportFlat.Web.server.system
{
    public partial class SystemPage : BaseHandler
    {
        /// <summary>
        /// 保存
        /// </summary>
        [UseDB(Transaction = true)]
        public void Save()
        {
            t_system obj = ToPo<t_system>();
            if (obj.FID == 0)
            {
                dao.Save(obj);
            }
            else
            {
                dao.Update(obj);
            }
            jsonResult = JSONHelper.SuccessJson;
        }

        public void GetValue()
        {
            jsonResult = JSONHelper.ToSucJson("value", bussiness.BusSystem.getValue(Request["key"]));
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        [UseDB]
        public void GetList()
        {
            int start = int.Parse(Request.Params["start"].ToString());
            int limit = int.Parse(Request.Params["limit"].ToString());
            string sWhere = Request.Params["where"].ToString();
            int rowCount;
            IList<t_system> list = dao.GetList<t_system>(out rowCount, sWhere, "", start, limit);
            jsonResult = JSONHelper.ToSucJson((IList)list, rowCount);
        }

        [UseDB]
        public void GetArea()
        {
            int pid = -1;
            if (!String.IsNullOrEmpty(Request["pid"]))
            {
                pid = Convert.ToInt32(Request["pid"]);
            }
            IList<t_areas> list = dao.QueryByHSql<t_areas>("from t_areas where FParentid=" + pid + " ORDER BY FSort ");
            jsonResult = JSONHelper.ToSucJson("options", (IList)list);
        }
    }
}