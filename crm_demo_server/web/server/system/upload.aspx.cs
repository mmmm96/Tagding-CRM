﻿using ReportFlat.bussiness;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace ReportFlat.Web
{
    public partial class upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //to-do: 存在严重的安全隐患,
            //to-do: 要设置目录upload不能iis访问
            //to-do: 同时要加入权限判断,加入后缀判断
            string filename = Upload();
            Response.Write(JSONHelper.ToSucJson("filename", filename));
        }

        //上传附件
        private string Upload()
        {
            string newFilesName = "";
            //遍历File表单元素 (现在只支持一个附件 )  
            if (Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
            {
                HttpPostedFile postedFile = Request.Files[0];
                string Extension = Path.GetExtension(postedFile.FileName);
                if (Extension.ToLower() == ".exe" || Extension.ToLower() == ".aspx" || Extension.ToLower() == ".asp")
                {
                    throw new Exception("非法文件！");
                }
                newFilesName = Guid.NewGuid().ToString() + Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(Const.WebPath + "\\upload\\" + newFilesName);
            }
            return newFilesName;
        }
    }
}