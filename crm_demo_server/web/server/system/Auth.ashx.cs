﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;

namespace ReportFlat.Web.server.system
{
    /// <summary>
    /// Role 的摘要说明
    /// </summary>
    public class Auth : BaseHandler
    {
        [UseDB]
        public void GetMenu()
        {
            int roleId = -1;
            if (Request.Params["roleId"] != null)
            {
                roleId = Convert.ToInt32(Request.Params["roleId"]);
            }
            IList<t_menu> list = ReportFlat.bussiness.CacheManager.instance.GetMenuAll();
            //构造树形结构
            IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
            MakeTree(roleId, list, 0, jsonList);
            jsonResult = JSONHelper.ToSucJson("menu",jsonList);
        }

        [UseDB(Transaction=true)]
        public void SaveRoleAccess()
        {
            int roleId = Convert.ToInt32(Request.Params["roleId"]);
            IList<t_role_aceess> lstSave = JSONHelper.ToObject<IList<t_role_aceess>>(Request.Params["saveData"]);
            //先删除
            dao.ExcuteByHSql("delete from t_role_aceess where FRoleID=" + roleId);
            //后保存
            foreach (t_role_aceess obj in lstSave)
            {
                dao.Save(obj);
            }
            //刷新缓存
            ReportFlat.bussiness.CacheManager.instance.RefrashRoleAccess(roleId);
            jsonResult = JSONHelper.SuccessJson;
        }

        private int MakeTree(int roleId, IList<t_menu> listTree, int parent, IList<Dictionary<string, object>> outputTree)
        {
            int childCount = 0;
            foreach (t_menu menu in listTree)
            {
                if (menu.FParent == parent)
                {
                    if (!menu.FVisable_Vue)
                    {
                        continue;
                    }
                    childCount++;
                    Dictionary<string, object> josonDict = new Dictionary<string, object>();
                    josonDict.Add("FID", menu.FID);
                    josonDict.Add("FMenuID", menu.FID);
                    josonDict.Add("FName", menu.FName);
                    josonDict.Add("FParent", menu.FParent);
                    josonDict.Add("checked", isHasAccess(menu.FID, roleId));
                    IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
                    int myChildCount = MakeTree(roleId, listTree, menu.FID, jsonList);
                    if (myChildCount > 0)
                    {
                        josonDict.Add("children", jsonList);
                    }
                    outputTree.Add(josonDict);
                }
            }
            return childCount;
        }

        private ISet<int> RoleAccessSet = null;
        private bool isHasAccess(int menuId, int roleId)
        {
            if (roleId == -1)
            {
                return true;
            }
            if (RoleAccessSet == null)
            {
                //使用set加快速度
                RoleAccessSet = ReportFlat.bussiness.CacheManager.instance.GetRoleAccessSet(roleId);
            }
            return RoleAccessSet.Contains(menuId);
        }
    }
}