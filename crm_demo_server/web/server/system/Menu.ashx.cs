﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportFlat.Common;
using ReportFlat.DataAccess;
using System.Collections;
namespace ReportFlat.Web.server.system
{
    /// <summary>
    /// Menu 的摘要说明
    /// </summary>
    public class Menu : BaseHandler
    {
        /// <summary>
        /// 保存菜单
        /// </summary>
        [UseDB(Transaction = true)]
        public void SaveMenu()
        {
            t_menu obj = ToPo<t_menu>();
            if (obj.FID == 0)
            {
                obj.FID = dao.Save(obj);
                //默认加查看、修改、删除权限
                //系统设置不用加
                if (obj.FLevel == 2 && obj.FParent!=1) 
                {
                    t_menu view = new t_menu();
                    view.FOrder = 2;
                    view.FParent = obj.FID;
                    view.FLevel = 3;
                    view.FName = "查看";
                    view.FUrl_Vue = "#";
                    view.FRequire_Vue = "view";
                    view.FVisable_Vue = true;

                    t_menu add = new t_menu();
                    add.FOrder = 3;
                    add.FParent = obj.FID;
                    add.FLevel = 3;
                    add.FName = "新增";
                    add.FUrl_Vue = "#";
                    add.FRequire_Vue = "add";
                    add.FVisable_Vue = true;

                    t_menu modify = new t_menu();
                    modify.FOrder = 4;
                    modify.FParent = obj.FID;
                    modify.FLevel = 3;
                    modify.FName = "修改";
                    modify.FUrl_Vue = "#";
                    modify.FRequire_Vue = "modify";
                    modify.FVisable_Vue = true;

                    t_menu delete = new t_menu();
                    delete.FOrder = 6;
                    delete.FParent = obj.FID;
                    delete.FLevel = 3;
                    delete.FName = "删除";
                    delete.FUrl_Vue = "#";
                    delete.FRequire_Vue = "delete";
                    delete.FVisable_Vue = true;

                    dao.Save(view);
                    dao.Save(modify);
                    dao.Save(add);
                    dao.Save(delete);
                }
            }
            else
            {   //更新子级的FModuleID
                if (obj.FLevel == 1)
                {
                    dao.ExcuteBySql("update t_menu set FModuleID="+obj.FModuleID+" where FParent in (SELECT FID FROM (select FID from t_menu where FParent=" + obj.FID + ") A )");
                }
                if (obj.FLevel == 1 || obj.FLevel == 2)
                {
                    dao.ExcuteByHSql("update t_menu set FModuleID=" + obj.FModuleID + " where FParent=" + obj.FID);
                }
                dao.Update(obj);
            }
            jsonResult = JSONHelper.ToSucJson("FID", obj.FID);
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        [UseDB(Transaction = true)]
        public void DeleteMenu()
        {
            //删除子节点，删除权限项
            int id = Convert.ToInt32(Request["FID"]);
            t_menu menu = dao.Get<t_menu>(id);
            if (menu.FLevel == 1)
            {
                dao.ExcuteBySql("delete from t_menu where FParent in (select FID FROM (select FID from t_menu where FParent=" + id + ") A)");
                dao.ExcuteByHSql(@"delete from t_role_aceess where FMenuID 
                                           in(select FID from t_menu where FParent 
                                                          in(select FID from t_menu where FParent=" + id + "))"
                                );
            }
            if (menu.FLevel == 1 || menu.FLevel == 2)
            {
                dao.ExcuteByHSql("delete from t_menu where FParent=" + id);
                dao.ExcuteByHSql("delete from t_role_aceess where FMenuID in(select FID from t_menu where FParent=" + id + ")");
            }
            dao.Delete(menu);
            dao.ExcuteByHSql("delete from t_role_aceess where FMenuID=" + id);
            jsonResult = JSONHelper.SuccessJson;
        }

        /// <summary>
        /// 获取菜单列表
        /// </summary>
        public void GetMenuTree()
        {
            int ModuleID = Convert.ToInt32(Request["FModuleID"]);
            IList<t_menu> list = ReportFlat.bussiness.CacheManager.instance.GetMenuAll();
            IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
            MakeTree(list, 0, ModuleID, jsonList);
            jsonResult = JSONHelper.ToSucJson("data", jsonList);
        }

        private int MakeTree(IList<t_menu> listTree, int parent,int ModuleID, IList<Dictionary<string, object>> outputTree)
        {
            int childCount = 0;
            foreach (t_menu item in listTree)
            {
                if (item.FParent == parent && (ModuleID == 0 || item.FModuleID == ModuleID))
                {
                    childCount++;
                    Dictionary<string, object> josonDict = new Dictionary<string, object>();
                    josonDict.Add("FID", item.FID);
                    josonDict.Add("FName", item.FName);
                    josonDict.Add("FOrder", item.FOrder);
                    josonDict.Add("FParent", item.FParent);
                    josonDict.Add("FIconCls_Vue", item.FIconCls_Vue);
                    josonDict.Add("FQuery", item.FQuery);
                    josonDict.Add("FUrl_Vue", item.FUrl_Vue);
                    josonDict.Add("FRequire_Vue", item.FRequire_Vue);
                    josonDict.Add("FVisable_Vue", item.FVisable_Vue);
                    josonDict.Add("FLevel", item.FLevel);
                    josonDict.Add("FModuleID", item.FModuleID);
                    IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
                    int myChildCount = MakeTree(listTree, item.FID, ModuleID,jsonList);
                    if (myChildCount > 0)
                    {
                        josonDict.Add("children", jsonList);
                    }
                    outputTree.Add(josonDict);
                }
            }
            return childCount;
        }
    }
}