﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ReportFlat.bussiness;
using ReportFlat.DataAccess;
using ReportFlat.Common;
using System.Collections.Generic;

namespace ReportFlat.Web.server.system
{
    /// <summary>
    /// Main 的摘要说明
    /// </summary>
    public class Main : BaseHandler
    {
        [NoNeedLogin]
        public void Init()
        {
            IList<t_menu> list = CacheManager.instance.GetMenuAll();
            //构造树形结构
            IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
            MakeTree(list, 0, jsonList);
            IList<t_module> modulelist = CacheManager.instance.GetModuleAll();
            MLogin GMLogin = (MLogin)Session["login"];
            jsonResult = JSONHelper.AddF("menu", jsonList)
                .AddF("user", GMLogin == null ? null : GMLogin.User)
                .AddF("roles", GetRoles(GMLogin))
                .AddF("authority", GetAuthority(GMLogin))
                .AddF("sysParam", SysParam())
                .AddF("module", modulelist)
                .ToSucJson();
        }

        private Dictionary<string,string> SysParam(){
            Dictionary<string, string> result = new Dictionary<string, string>();
            result.Add("oneToOne", BusSystem.getValue("oneToOne"));
            return result;
        }

        private IList<int> GetRoles(MLogin GMLogin)
        {
            if (GMLogin == null)
            {
                return null;
            }
            IList<int> result = new List<int>();
            if (GMLogin.isAdminUser())
            {
                result.Add(-1);
                return result;
            }
            if (GMLogin.User != null && GMLogin.User.Roles != null && GMLogin.User.Roles.Count > 0)
            {
                foreach (t_role role in GMLogin.User.Roles)
                {
                    //to-do:属于多个role时，去重
                    result.Add(role.FID);
                }
            }
            return result;
        }

        private IList<int> GetAuthority(MLogin GMLogin)
        {
            if (GMLogin == null)
            {
                return null;
            }
            IList<int> result = new List<int>();
            if (GMLogin.isAdminUser())
            {
                result.Add(-1);
                return result;
            }
            if (GMLogin.User != null && GMLogin.User.Roles != null && GMLogin.User.Roles.Count > 0)
            {
                foreach (t_role role in GMLogin.User.Roles)
                {
                    foreach (t_role_aceess role_aceess in CacheManager.instance.GetRoleAccess(role.FID))
                    {
                        //to-do:属于多个role时，去重
                        result.Add(role_aceess.FMenuID);
                    }
                }
            }
            return result;
        }

        private int MakeTree(IList<t_menu> listTree, int parent, IList<Dictionary<string, object>> outputTree)
        {
            int childCount = 0;
            foreach (object obj in listTree)
            {
                t_menu menu = (t_menu)obj;
                if (menu.FParent == parent)
                {
                    if (!menu.FVisable_Vue)
                    {
                        continue;
                    }
                    childCount++;
                    Dictionary<string, object> josonDict = new Dictionary<string, object>();
                    josonDict.Add("id", menu.FID);
                    josonDict.Add("name", menu.FName);
                    josonDict.Add("path", menu.FUrl_Vue);
                    josonDict.Add("query", menu.FQuery);
                    josonDict.Add("componentPath", menu.FRequire_Vue);
                    josonDict.Add("iconCls", menu.FIconCls_Vue);
                    josonDict.Add("parent", menu.FID);
                    josonDict.Add("level", menu.FLevel);
                    josonDict.Add("moduleID", menu.FModuleID);
                    josonDict.Add("show",false);
                    IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
                    int myChildCount = MakeTree(listTree, menu.FID, jsonList);
                    if (myChildCount > 0)
                    {
                        //只支持2层菜单，第三层是功能权限项
                        if (menu.FUrl_Vue == "/")
                        {
                            josonDict.Add("children", jsonList);
                        }
                        else
                        {
                            josonDict.Add("authority", jsonList);
                        }
                    }
                    outputTree.Add(josonDict);
                }
            }
            return childCount;
        }
    }
}