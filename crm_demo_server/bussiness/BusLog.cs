﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReportFlat.DataAccess;


namespace ReportFlat.bussiness
{
    public class BusLog : BaseBus
    {
        //获取日志分页
        public IList GetList(string sWhere, int start, int limit, out int rowCount)
        {
            return (IList)BusTool.GetList<t_log>(out rowCount, sWhere, "FCreateDate desc", start, limit);
        }

        //业务级日志(写到数据库)
        public static void Save(t_log log)
        {
            BusTool.Save(log);
        }

        //系统级，写到文件
        public static void WriteErrorLog(object message, Exception e, string logger = "myLog")
        {
            log4net.ILog log = log4net.LogManager.GetLogger("myLog");
            log.Error(message, e);
        }

        //系统级，写到文件
        public static void WriteErrorLog(object message, string logger = "myLog")
        {
            log4net.ILog log = log4net.LogManager.GetLogger(logger);
            log.Error(message);
        }

        public static void WriteInfoLog(object message, string logger = "myLog")
        {
            log4net.ILog log = log4net.LogManager.GetLogger("myLog");
            log.Info(message);
        }
    }
}
