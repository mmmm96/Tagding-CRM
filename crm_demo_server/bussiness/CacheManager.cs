﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
using ReportFlat.DataAccess;

namespace ReportFlat.bussiness
{
    public class CacheManager 
    {
        public static CacheManager instance = new CacheManager();
        private IList<t_menu> listMenu = new List<t_menu>();
        private Dictionary<int, IList<t_role_aceess>> dicRoleAccess
            = new Dictionary<int, IList<t_role_aceess>>();

        //to-do:改使用system.web.caching.cache，增加过期策略,并设置超大时存到文件中
        private CacheManager()
        {
        }

        //角色权限缓存
        public void RefrashRoleAccess(int RoleID)
        {
            if (dicRoleAccess.ContainsKey(RoleID))
            {
                dicRoleAccess.Remove(RoleID);
            }
            GetRoleAccess(RoleID);
        }

        public IList<t_role_aceess> GetRoleAccess(int RoleID)
        {

            return BusTool.GetList<t_role_aceess>("FRoleID=" + RoleID, "FMenuID");
            /*
                    lock (dicRoleAccess)
                    {
                        if (!dicRoleAccess.ContainsKey(RoleID))
                        {
                            IList<t_role_aceess> list = BusTool.GetList<t_role_aceess>("FRoleID=" + RoleID, "FMenuID");
                            dicRoleAccess.Add(RoleID, list);
                        }
                    }
                    return dicRoleAccess[RoleID];
            */
        }

        public ISet<int> GetRoleAccessSet(int RoleID)
        {
            IList<t_role_aceess> list = CacheManager.instance.GetRoleAccess(RoleID);
            ISet<int> set = new HashSet<int>();
            foreach(t_role_aceess aceess in list){
                set.Add(aceess.FMenuID);
            }
            return set;
         }

        //菜单缓存
        public void RefrashMenuAll()
        {
            if (listMenu.Count>0)
            {
                listMenu.Clear();
            }
            GetMenuAll();
        }

        public IList<t_menu> GetMenuAll()
        { 
             return BusTool.GetList<t_menu>("","FParent,FOrder");
             /*
                lock (dicIList)
                {
                    if (!dicIList.ContainsKey("menuAll"))
                    {
                        dicIList.Add("menuAll", BusTool.GetList<t_menu>("","FParent,FOrder"));
                    }
                }
                return dicIList["menuAll"];
          */
        }

        public IList<t_module> GetModuleAll()
        {
            return BusTool.GetList<t_module>("", "FSort");
            /*
               lock (dicIList)
               {
                   if (!dicIList.ContainsKey("menuAll"))
                   {
                       dicIList.Add("menuAll", BusTool.GetList<t_menu>("","FParent,FOrder"));
                   }
               }
               return dicIList["menuAll"];
         */
        }


        public void Init()
        {
            GetMenuAll();
            GetModuleAll();
        }

        public void Start()
        {
            new Thread(new ThreadStart(Init)).Start();
        }
    }
}
