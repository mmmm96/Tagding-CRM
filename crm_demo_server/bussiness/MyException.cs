﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReportFlat.bussiness
{
    public class MyException : Exception
    {
        public MyException(string message)
            : base(message)
        {

        }
    }

    public class ArgException : MyException
    {
        public ArgException(string message)
            : base(message)
        {

        }
    }
}
