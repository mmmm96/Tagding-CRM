﻿using ReportFlat.DataAccess;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReportFlat.bussiness
{
    public class BaseBus
    {
        private MLogin mLogin;
        public MLogin GMLogin
        {
            get { return mLogin; }
            set { mLogin = value; }
        }

        public BaseBus()
        {

        }

        public BaseBus(MLogin GMLogin)
        {
            this.GMLogin = GMLogin;
        }
    }
}
