﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReportFlat.DataAccess;
using System.Web;
using System.Collections;

namespace ReportFlat.bussiness
{
    [Serializable]
    public class MLogin 
    {
        private t_user mUser;
        public MLogin()
        {
        }

        public MLogin(t_user user)
        {
            //加上roleUser
            IList<t_roleuser> roleUsers = new List<t_roleuser>();
            DAOAssist dao = new DAOAssist();
            //用户
            List<int> roleIds = new List<int>();
            IList<t_roleuser> listUser = dao.GetList<t_roleuser>(String.Format("FUserID = {0}", user.FID));
            foreach (var obj in listUser)
            {
                roleIds.Add(obj.FRoleID);
            }
            string where = String.Format("FDeptID in (select FDeptID from t_dept_user where FUserID = {0})", user.FID);
            IList<t_roledept> listDept = dao.GetList<t_roledept>(where);
             foreach (var obj in listUser)
            {
                roleIds.Add(obj.FRoleID);
            }
            dao.CloseSession();
            this.User = user;
            if (roleIds.Count>0)
            {
               IList<t_role> roles = dao.GetList<t_role>("FID in(" + string.Join(",", roleIds.ToArray()) + ")");
               user.Roles = roles;
            }
        }

        public t_user User
        {
            get
            {
                return this.mUser;
            }
            set
            {
                this.mUser = value;
            }
        }

        public int UserID
        {
            get
            {
                return this.mUser.FID;
            }
        }

        public Boolean isRole(int roleID)
        {
            foreach (t_role role in User.Roles)
            {
                if (role.FID == roleID)
                {
                    return true;
                }
            }
            return false;
        }

        //是否管理员
        public Boolean isAdminUser()
        {
            if(UserID == -1){
                return true;
            }
            return isRole(-1);
        }
    }
}
