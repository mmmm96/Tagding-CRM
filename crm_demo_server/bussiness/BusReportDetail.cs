﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Weather.DataAccess;
using Weather.Common;


namespace Weather.bussiness
{
    public class BusReportDetail : InterfaceBus
    {
        //获取检测报告明细列表
        public IList GetList(string sWhere, int start, int limit,out int rowCount)
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_reportDetail where " + sWhere, start, limit, out rowCount);
            daoAssist.CloseSession();
            return list;
        }

        //获取检测报告明细列表
        public IList GetList(string sWhere) 
        {
            if (sWhere.Length == 0)
            {
                sWhere = "1=1";
            }
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_reportDetail where " + sWhere);
            daoAssist.CloseSession();
            return list;
        }

        //新增检测报告明细
        public void Save(object obj)
        {
            t_reportDetail reportDetail = (t_reportDetail)obj;
            DAOAssist daoAssist = new DAOAssist();
            try
            {
                daoAssist.BeginTransaction();
                daoAssist.Save(reportDetail);
                daoAssist.Commit();
            }
            catch (Exception ex)
            {
                daoAssist.Rollback();
                throw ex;
            } 
        }

        //更新检测报告明细
        public void Update(object obj)
        {
            t_reportDetail reportDetail = (t_reportDetail)obj;
            DAOAssist daoAssist = new DAOAssist();
            try
            {
                daoAssist.BeginTransaction();
                //t_reportDetail reportDetailTo = daoAssist.Load<t_reportDetail>(reportDetail.FID);
                daoAssist.Update(reportDetail);
                daoAssist.Commit();
            }
            catch (Exception ex)
            {
                daoAssist.Rollback();
                throw ex;
            }
        }

        //删除检测报告明细
        public void Delete(int noticeID)
        {
            DAOAssist daoAssist = new DAOAssist();
            daoAssist.BeginTransaction();
            try
            {
                t_reportDetail reportDetail = new t_reportDetail();
                reportDetail.FID = noticeID;
                daoAssist.Delete(reportDetail);
                daoAssist.Commit();
            }
            catch (Exception ex)
            {
                daoAssist.Rollback();
                throw ex;
            }
        }
    }
}
