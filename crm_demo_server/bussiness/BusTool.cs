﻿using ReportFlat.DataAccess;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ReportFlat.bussiness
{
    public delegate void Running(DAOAssist daoAssist);
    public delegate T Running<T>(DAOAssist daoAssist);
    public class BusTool
    {
        /// <summary>
        /// 静态工具方法
        /// </summary>
        /// <param name="obj"></param>
        public static int Save(Object obj)
        {
            int result = 0;
            BusTool.DBRun(x => {
                result = x.Save(obj);
            });
            return result;
        }

        public static void Update(Object obj)
        {
            BusTool.DBRun(x => { x.Update(obj); });
        }

        public static void Delete(Object obj)
        {
            BusTool.DBRun(x => { x.Delete(obj); });
        }

        public static void DBRun(Running run)
        {
            DAOAssist daoAssist = new DAOAssist();
            try
            {
                daoAssist.BeginTransaction();
                run(daoAssist);
                daoAssist.Commit();
            }
            catch (Exception ex)
            {
                daoAssist.Rollback();
                throw ex;
            }
            finally
            {
                daoAssist.CloseSession();
            }
        }

        public static T DBGet<T>(Running<T> run)
        {
            DAOAssist daoAssist = new DAOAssist();
            try
            {
               return run(daoAssist);
            }
            finally
            {
                daoAssist.CloseSession();
            }
        }

        public static void DBGet(Running run)
        {
            DAOAssist daoAssist = new DAOAssist();
            try
            {
                run(daoAssist);
            }
            finally
            {
                daoAssist.CloseSession();
            }
        }

        public static T DBGet<T>(int FID)
        {
            DAOAssist daoAssist = new DAOAssist();
            try
            {
                return daoAssist.Get<T>(FID);
            }
            finally
            {
                daoAssist.CloseSession();
            }
        }

        public static T DBGet<T>(string condition)
        {
            DAOAssist daoAssist = new DAOAssist();
            try
            {
                return daoAssist.Get<T>(condition);
            }
            finally
            {
                daoAssist.CloseSession();
            }
        }

        //获取系统参数列表
        public static IList<T> GetList<T>(out int rowCount,string sWhere, string orderBy, int start, int limit)
        {
            DAOAssist daoAssist = new DAOAssist();
            IList<T> list = daoAssist.GetList<T>(out rowCount, sWhere, orderBy, start, limit);
            daoAssist.CloseSession();
            return list;
        }

        public static IList<T> GetList<T>(out int rowCount, string sWhere, string orderBy)
        {
            return BusTool.GetList<T>(out rowCount, sWhere, orderBy, 0, 0);
        }

        public static IList<T> GetList<T>(string sWhere, string orderBy, int start, int limit)
        {
            DAOAssist daoAssist = new DAOAssist();
            IList<T> list = daoAssist.GetList<T>(sWhere, orderBy, start, limit);
            daoAssist.CloseSession();
            return list;
        }

        public static IList<T> GetList<T>(string sWhere, string orderBy)
        {
            return BusTool.GetList<T>(sWhere, orderBy, 0, 0);
        }

        public IList<T> QueryByHSql<T>(string hSql)
        {
            DAOAssist daoAssist = new DAOAssist();
            IList<T> list = daoAssist.QueryByHSql<T>(hSql);
            daoAssist.CloseSession();
            return list;
        }
    }
}
