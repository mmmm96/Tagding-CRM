﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Data;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReportFlat.Web;
using ReportFlat.Common;
using ReportFlat.bussiness;


namespace ReportFlat.TestUnit.Web
{
    /// <summary>
    /// TestMain 的摘要说明
    /// </summary>
    [TestClass]
    public class TestMain
    {
        public TestMain()
        {
            ReportFlat.TestUnit.Common.Common.init();
        }

        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，该上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        //
        // 编写测试时，可以使用以下附加特性:
        //
        // 在运行类中的第一个测试之前使用 ClassInitialize 运行代码
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // 在类中的所有测试都已运行之后使用 ClassCleanup 运行代码
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // 在运行每个测试之前，使用 TestInitialize 来运行代码
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // 在每个测试运行完之后，使用 TestCleanup 来运行代码
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {

        }


        [TestMethod]
        public void TestRecharge()
        {

        }

        [TestMethod]
        public void TestMenuTree()
        {
            //构造树形结构
            IList<Dictionary<string, object>> jsonListRoot = new List<Dictionary<string, object>>();
            Dictionary<string, object> josonDict = new Dictionary<string, object>();
            josonDict.Add("id", 1);
            josonDict.Add("text", "根1");
            josonDict.Add("expanded", true);
            josonDict.Add("pid", 0);
            josonDict.Add("checked", false);
            josonDict.Add("cls", "folder");
            jsonListRoot.Add(josonDict);

            IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
            josonDict.Add("children", jsonList);
            josonDict = new Dictionary<string, object>();
            jsonList.Add(josonDict);

            josonDict.Add("id", 2);
            josonDict.Add("text", "根2");
            josonDict.Add("expanded", true);
            josonDict.Add("pid", 1);
            josonDict.Add("checked", false);
            josonDict.Add("cls", "folder");

            josonDict = new Dictionary<string, object>();
            jsonList.Add(josonDict);

            jsonList = new List<Dictionary<string, object>>();
            josonDict.Add("children", jsonList);
            josonDict = new Dictionary<string, object>();
            jsonList.Add(josonDict);

            josonDict.Add("id", 3);
            josonDict.Add("text", "子节点3");
            josonDict.Add("expanded", true);
            josonDict.Add("pid", 2);
            josonDict.Add("cls", "folder");
            josonDict.Add("checked", false);
            //josonDict.Add("leaf", true);

            jsonList = new List<Dictionary<string, object>>();
            josonDict.Add("children", jsonList);

            josonDict = new Dictionary<string, object>();
            jsonList.Add(josonDict);
            josonDict.Add("id", 4);
            josonDict.Add("text", "子节点4");
            josonDict.Add("leaf", true);
            josonDict.Add("checked", false);

            josonDict = new Dictionary<string, object>();
            jsonList.Add(josonDict);
            josonDict.Add("id", 5);
            josonDict.Add("text", "子节点5");
            josonDict.Add("leaf", true);
            josonDict.Add("checked", false);

            josonDict = new Dictionary<string, object>();
            jsonList.Add(josonDict);
            josonDict.Add("id", 6);
            josonDict.Add("text", "子节点6");
            josonDict.Add("leaf", true);
            josonDict.Add("checked", false);
             Console.Out.WriteLine(JSONHelper.ToJSON(jsonListRoot));
        }

    }
}
