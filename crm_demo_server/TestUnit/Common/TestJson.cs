﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReportFlat.Common;

namespace ReportFlat.TestUnit.Common
{
    /// <summary>
    /// TestJson 的摘要说明
    /// </summary>
    [TestClass]
    public class TestJson
    {
        public TestJson()
        {
            //
            //TODO: 在此处添加构造函数逻辑
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，该上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        //
        // 编写测试时，可以使用以下附加特性:
        //
        // 在运行类中的第一个测试之前使用 ClassInitialize 运行代码
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // 在类中的所有测试都已运行之后使用 ClassCleanup 运行代码
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // 在运行每个测试之前，使用 TestInitialize 来运行代码
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // 在每个测试运行完之后，使用 TestCleanup 来运行代码
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            IList<Dictionary<string, object>> jsonList = new List<Dictionary<string, object>>();
            Dictionary<string,object> dict = new Dictionary<string,object>();
            dict.Add("fid",1);
            dict.Add("fname", "pkkkkk");
            dict = new Dictionary<string, object>();
            jsonList.Add(dict);
            dict.Add("fid", 2);
            dict.Add("fname", "pkkkkk222");
            jsonList.Add(dict);
            Console.WriteLine(JSONHelper.ToJSON(jsonList));
        }

        [TestMethod]
        public void TestMethod2()
        {
            string sTest1 = "{FID:1,FName:'no1'}";
            string sTest2 = "[{FID:1,FName:'no1'},{FID:2,FName:'no2'},{FID:2,FName:'no2'}]";
            IList<Dictionary<string, object>> dicOut = JSONHelper.ToObject<IList<Dictionary<string, object>>>(sTest2);
            Console.WriteLine(JSONHelper.ToJSON(dicOut[0]["FID"]));
        }
    }
}
