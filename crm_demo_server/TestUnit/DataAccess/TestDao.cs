﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReportFlat.DataAccess;
using System.Collections;
using ReportFlat.Common;
using System;

namespace ReportFlat.TestUnit.DataAccess
{
    /// <summary>
    /// TestMenu 的摘要说明
    /// </summary>
    [TestClass]
    public class TestDao
    {
        public TestDao()
        {
            ReportFlat.TestUnit.Common.Common.init();
        }

        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，该上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        //
        // 编写测试时，可以使用以下附加特性:
        //
        // 在运行类中的第一个测试之前使用 ClassInitialize 运行代码
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // 在类中的所有测试都已运行之后使用 ClassCleanup 运行代码
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // 在运行每个测试之前，使用 TestInitialize 来运行代码
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // 在每个测试运行完之后，使用 TestCleanup 来运行代码
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public void TestOrder()
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_sendBus");
            daoAssist.CloseSession();
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void TestOrderDetail()
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_orderDetail");
            daoAssist.CloseSession();
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void TestUser()
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_user");
            daoAssist.CloseSession();
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void TestRole()
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_role");
            daoAssist.CloseSession();
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void TestRoleUser()
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("select * from t_roleuser");
            //t_user user = (t_user)list[0];
            Console.WriteLine(list.Count);
            Assert.IsTrue(list.Count > 0);
            daoAssist.CloseSession();
        }

        [TestMethod]
        public void TestDept()
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_department");
            daoAssist.CloseSession();
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void TestMenu()
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_menu");
            daoAssist.CloseSession();
            Assert.IsTrue(list.Count>0);
        }

        [TestMethod]
        public void TestMenuAccess()
        {
            DAOAssist daoAssist = new DAOAssist();
            IList list = daoAssist.QueryByHSql("from t_menu_aceess");
            daoAssist.CloseSession();
            Assert.IsTrue(list.Count > 0);
        }

        [TestMethod]
        public void TestInsert()
        {
            t_roleuser role = new t_roleuser();
            //role.FName = "test";
            role.FUserID = 100;
            role.FRoleID = 1315;
            DAOAssist daoAssist = new DAOAssist();
            try
            {
                daoAssist.BeginTransaction();
                daoAssist.Save(role);
                daoAssist.Commit();
            }
            catch (Exception ex)
            {
                daoAssist.Rollback();
                throw ex;
            }
            finally
            {
                daoAssist.CloseSession();
            }
        }

        [TestMethod]
        public void TestDelete()
        {
            DAOAssist daoAssist = new DAOAssist();
            daoAssist.BeginTransaction();
            try
            {
                t_menu menu = new t_menu();
                menu.FID = 146;
                daoAssist.Delete(menu);
                daoAssist.Commit();
            }
            catch (Exception ex)
            {
                daoAssist.Rollback();
                throw ex;
            }
            finally
            {
                daoAssist.CloseSession();
            }
        }
    }
}
