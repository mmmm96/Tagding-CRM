﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace ReportFlat.Common
{

    //声明扩展方法
    //要扩展的方法必须是静态的方法，Add有三个参数
    //this 必须有，string表示我要扩展的类型，stringName表示对象名
    //三个参数this和扩展的类型必不可少，对象名可以自己随意取如果需要传递参数，再增加一个变量即可
    public static class ExtensionString
    {
        //数据库SQL的使用类，
        //TODO:有点隐晦，最好重构
        public static string wrap(this String stuName,string str="'")
        {
            return str + stuName + str;
        }

        public static string like(this String stuName)
        {
            return "'%" + stuName + "%'";
        }

        public static string likeAll<T>(this String stuName)
        {
            string result="";
            Type type = typeof(T);
            PropertyInfo[] props = type.GetProperties();
            foreach (PropertyInfo prop in props)
            {
                if (prop.Name.StartsWith("F") && prop.PropertyType == typeof(string))
                {
                    if (result!="")
                    {
                        result += " or ";
                    }
                    result += prop.Name + " like " + stuName.like();
                }
            }
            if (result != "")
            {
                result = "(" + result + ")";
            }
            return result;
        }

        public static string and(this String stuName,string where)
        {
            if (string.IsNullOrEmpty(where))
            {
                return stuName;
            }
            else if (string.IsNullOrEmpty(stuName))
            {
                return where;
            }
            else
            {
                return stuName + " and " + where;
            }
        }
    }

    public static class Tools
    {
        public static bool isNumberic(string message)
        {
            int result = 0;
            System.Text.RegularExpressions.Regex rex =
            new System.Text.RegularExpressions.Regex(@"^\d+$");
            result = -1;
            if (rex.IsMatch(message))
            {
                result = int.Parse(message);
                return true;
            }
            else
                return false;
        }

        //从字符串中取数字
        public static int GetNumFromStr(string str){
            string result = "";
            foreach (char c in str)
            {
                if (Char.IsNumber(c))
                {
                    result += c;
                }
            }
            if (result.Length > 0)
            {
                return int.Parse(result);
            }
            else
            {
                return 0;
            }
        }

        #region 请求http
        public static string GetPage(string getUrl)
        {
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            // 准备请求...
            try
            {
                // 设置参数
                request = WebRequest.Create(getUrl) as HttpWebRequest;
                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "GET";
                request.Headers.Add("Accept-Language", "zh-CN,zh;q=0.8");
                request.ContentType = "text/plain;charset=UTF-8";
                //request.ContentType = "application/x-www-form-urlencoded";
                //发送请求并获取相应回应数据
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码
                string content = sr.ReadToEnd();
                string err = string.Empty;
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }

        //格式化时间
        public static string GetPage(string posturl, string postData)
        {
            Stream outstream = null;
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            byte[] data = encoding.GetBytes(postData);
            // 准备请求...
            try
            {
                // 设置参数
                request = WebRequest.Create(posturl) as HttpWebRequest;
                CookieContainer cookieContainer = new CookieContainer();
                request.CookieContainer = cookieContainer;
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.Headers.Add("Accept-Language", "zh-CN,zh;q=0.8");
                request.ContentType = "text/plain;charset=UTF-8";
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                outstream = request.GetRequestStream();
                outstream.Write(data, 0, data.Length);
                outstream.Close();
                //发送请求并获取相应回应数据
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码
                string content = sr.ReadToEnd();
                string err = string.Empty;
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }

        public static string GetPageCookie(string url, string cookie)
        {
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            // 准备请求...
            try
            {
                // 设置参数
                request = WebRequest.Create(url) as HttpWebRequest;
                request.Headers.Add("Cookie",cookie);
                request.AllowAutoRedirect = true;
                request.Method = "GET";
                request.Headers.Add("Accept-Language", "zh-CN,zh;q=0.8");
                request.ContentType = "text/plain;charset=UTF-8";
                //request.ContentType = "application/x-www-form-urlencoded";
                //发送请求并获取相应回应数据
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码
                string content = sr.ReadToEnd();
                string err = string.Empty;
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }

        public static string GetPageCookie(string posturl, string cookie, string postData)
        {
            Stream outstream = null;
            Stream instream = null;
            StreamReader sr = null;
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            Encoding encoding = Encoding.UTF8;
            byte[] data = encoding.GetBytes(postData);
            // 准备请求...
            try
            {
                // 设置参数
                request = WebRequest.Create(posturl) as HttpWebRequest;
                request.Headers.Add("Cookie", cookie);
                request.AllowAutoRedirect = true;
                request.Method = "POST";
                request.Headers.Add("Accept-Language", "zh-CN,zh;q=0.8");
                request.ContentType = "text/plain;charset=UTF-8";
                //request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;
                outstream = request.GetRequestStream();
                outstream.Write(data, 0, data.Length);
                outstream.Close();
                //发送请求并获取相应回应数据
                response = request.GetResponse() as HttpWebResponse;
                //直到request.GetResponse()程序才开始向目标网页发送Post请求
                instream = response.GetResponseStream();
                sr = new StreamReader(instream, encoding);
                //返回结果网页（html）代码
                string content = sr.ReadToEnd();
                string err = string.Empty;
                return content;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
                return string.Empty;
            }
        }
        #endregion
    }
}
