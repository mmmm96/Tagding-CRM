﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReportFlat.Common
{
    public static class Const
    {
        public static string SqlConn
        {
            get;
            set;
        }

        public static string WebPath
        {
            get;
            set;
        }

        public static string WebHost
        {
            get;
            set;
        }

        /// <summary>
        /// 微信参数
        /// </summary>
        public static string AppID
        {
            get;
            set;
        }

        public static string AppSecret
        {
            get;
            set;
        }
        public static string Token
        {
            get;
            set;
        }
    }

    //消息状态   ----0已读，1未读
    public enum MessageStatus : int
    {
        UnRead = 0,
        Readed = 1
    }

    public enum OrderStatus : int
    {
        Submit = 0,
        Pay = 1,
        Delivery = 2,
        Close = 3
    }

    public enum PayType : int
    {
        JF = 1,
        WX = 2
    }
}
