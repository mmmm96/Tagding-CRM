﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Web.Security;

namespace ReportFlat.Common
{
    public class Encryption
    {

        private const string MY_KEY = "HwagainA20121111";

        public static string HJMD5(string password)
        {
            return FormsAuthentication.HashPasswordForStoringInConfigFile(password, "MD5").ToLower();
        }

        public static string AESEncrypt(string strToEncrypt, string AES_KEY = MY_KEY)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(AES_KEY);
            byte[] encrypStrArray = UTF8Encoding.UTF8.GetBytes(strToEncrypt);

            RijndaelManaged r = new RijndaelManaged();
            r.Key = keyArray;
            r.Mode = CipherMode.ECB;
            r.Padding = PaddingMode.PKCS7;

            ICryptoTransform cryp = r.CreateEncryptor();
            byte[] resultArray = cryp.TransformFinalBlock(encrypStrArray, 0, encrypStrArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string AESDecrypt(string strToDecrypt, string AES_KEY = MY_KEY)
        {
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes(AES_KEY);
            byte[] decryptArray = Convert.FromBase64String(strToDecrypt);

            RijndaelManaged r = new RijndaelManaged();
            r.Key = keyArray;
            r.Mode = CipherMode.ECB;
            r.Padding = PaddingMode.PKCS7;

            ICryptoTransform cryp = r.CreateDecryptor();
            byte[] resultArray = cryp.TransformFinalBlock(decryptArray, 0, decryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }


        /// <summary>
        /// DES 加密过程
        /// </summary>
        /// <param name="pToDecrypt">被解密的字符串</param>
        /// <returns>返回被解密的字符串</returns>
        public static string Encrypt(string pToEncrypt, string sKey)
        {
            //访问数据加密标准(DES)算法的加密服务提供程序 (CSP) 版本的包装对象
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);　//建立加密对象的密钥和偏移量
            des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);　 //原文使用ASCIIEncoding.ASCII方法的GetBytes方法
            byte[] inputByteArray = Encoding.UTF8.GetBytes(pToEncrypt);//把字符串放到byte数组中
            MemoryStream ms = new MemoryStream();//创建其支持存储区为内存的流　
            //定义将数据流链接到加密转换的流
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            //上面已经完成了把加密后的结果放到内存中去
            StringBuilder ret = new StringBuilder();
            foreach (byte b in ms.ToArray())
            {
                ret.AppendFormat("{0:X2}", b);
            }
            ret.ToString();
            return ret.ToString();
        }
        /// <summary>
        /// DES 解密过程
        /// </summary>
        /// <param name="pToDecrypt">被解密的字符串</param>
        /// <returns>返回被解密的字符串</returns>
        public static string Decrypt(string pToDecrypt, string sKey)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = new byte[pToDecrypt.Length / 2];
            for (int x = 0; x < pToDecrypt.Length / 2; x++)
            {
                int i = (Convert.ToInt32(pToDecrypt.Substring(x * 2, 2), 16));
                inputByteArray[x] = (byte)i;
            }
            des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);　//建立加密对象的密钥和偏移量，此值重要，不能修改
            des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            //建立StringBuild对象，createDecrypt使用的是流对象，必须把解密后的文本变成流对象
            StringBuilder ret = new StringBuilder();
            return System.Text.Encoding.UTF8.GetString(ms.ToArray());
        }
    }
}
