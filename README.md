# Tagding-CRM

#### 项目介绍
性价比超高的CRM管理系统支持免费定制
QQ：20975212
tel：18376668806

#### 软件架构
前端：vuejs + elementui 
后端：C# donet

#### 安装教程
1、执行数据库脚本 db.sql
2、前端运行 install.bat 再运行 build.bat
3、把前端的 dist目录覆盖到后端web中
3. 使用iis指向web目录

#### 使用说明

1. 请参照 http://nn3600.com/crm.html

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)